DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
BIN_DIR=$DIR/bin
BIN_FILE=gpc
PKG_NAME=go_plagiarism_checker
SRC_DIR=$DIR/src
PKG_SRC_DIR=$SRC_DIR/$PKG_NAME
TEMPLATES_DIR=templates

DEPENDENCIES=(
	github.com/davecgh/go-spew/spew
	github.com/umpc/go-sortedmap
	golang.org/x/tools/go/ast/astutil
)

mkdir -p "$BIN_DIR"
export GOPATH=$DIR

for i in ${!DEPENDENCIES[@]}; do
	DEP=${DEPENDENCIES[$i]}
	if [ ! -d "$SRC_DIR/$DEP" ]; then
		go get -v $DEP
	fi
done

go build -o "$BIN_DIR/$BIN_FILE" "$PKG_NAME"

cp -r "$PKG_SRC_DIR/$TEMPLATES_DIR" "$BIN_DIR/$TEMPLATES_DIR"
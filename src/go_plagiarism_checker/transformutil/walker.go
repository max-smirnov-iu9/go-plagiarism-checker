package transformutil

import (
	"go/ast"
	"go/token"
	"reflect"
)

type TokenId struct {
	Parent ast.Node
	Index int
}

func ForEachToken(node ast.Node, callback func(TokenId)) {

	structure := reflect.ValueOf(node)
	forEachTokenInStructure(structure, callback, nil)

}

func forEachTokenInStructure(structure reflect.Value, callback func (TokenId), parent ast.Node) {

	if structure.IsNil() {
		return
	}

	nodeInterface := structure.Interface()
	node := nodeInterface.(ast.Node)
	structure = resolve(structure)

	// Some nodes have lists in them, in which subnodes are supposed to be comma separated
	restoreListCommas := false
	restoreListCommasInFirstListOnly := false
	switch node.(type) {
	case *ast.Field: // Name
		restoreListCommas = true
	case *ast.FieldList: // List - may be separated with either semicolons or commas, depending on the parent
		switch parent.(type) {
		case *ast.FuncType:
			restoreListCommas = true
		case *ast.FuncDecl:
			restoreListCommas = true
		}
	case *ast.CompositeLit: // Elts
		restoreListCommas = true
	case *ast.CallExpr: // Args
		restoreListCommas = true
	case *ast.AssignStmt: // Lhs, Rhs
		restoreListCommas = true
	case *ast.ReturnStmt: // Results
		restoreListCommas = true
	case *ast.CaseClause: // List
		restoreListCommas = true
		restoreListCommasInFirstListOnly = true
	case *ast.ValueSpec: // Names, Values
		restoreListCommas = true
	}

	// Virtual indices of fields that have a missing token
	restoreTokenAt := make(map[int]bool)
	switch node := node.(type) {
	case *ast.SelectorExpr: // Missing: dot
		restoreTokenAt[1] = true
	case *ast.SliceExpr: // Missing: colons
		restoreTokenAt[3] = true
		if node.Slice3 {
			restoreTokenAt[5] = true
		}
	case *ast.TypeAssertExpr: // Missing: dot
		restoreTokenAt[1] = true
	case *ast.ArrayType: // Missing: right bracket
		restoreTokenAt[2] = true
	case *ast.MapType: // Missing: brackets
		restoreTokenAt[1] = true
		restoreTokenAt[3] = true
	case *ast.IfStmt:
		if node.Else != nil {
			restoreTokenAt[4] = true
		}
	case *ast.RangeStmt: // Missing: equals, range
		if node.Key != nil && node.Value != nil {
			restoreTokenAt[2] = true
		}
		restoreTokenAt[6] = true
	case *ast.ValueSpec: // Missing: equals
		if node.Values != nil {
			restoreTokenAt[3] = true
		}
	}

	fields := structure.NumField()
	virtualIndexMap := getVirtualFieldIndexMap(node)
	nextVirtualIndex := fields
	if virtualIndexMap != nil {
		nextVirtualIndex = virtualIndexMap[len(virtualIndexMap) - 1] + 1
	}

	isFirstList := true
	prevVirtualIndex := -1
	for i := 0; i < fields; i++ {

		// Special case: in function declarations, the `func` token is stored after the function name.
		if i == 0 {
			_, isFuncType := node.(*ast.FuncType)
			isParentFuncDecl := false
			if parent != nil && isFuncType {
				_, isParentFuncDecl = parent.(*ast.FuncDecl)
			}
			if isFuncType && isParentFuncDecl {
				continue
			}
		}

		// Special case: in File, the imports are stored second time after declarations
		if i > 3 {
			if _, isFile := node.(*ast.File); isFile {
				break
			}
		}

		virtualIndex := i
		if virtualIndexMap != nil && prevVirtualIndex != -1 {
			virtualIndex = virtualIndexMap[i]
			for j := prevVirtualIndex + 1; j < virtualIndex; j++ {
				if _, ok := restoreTokenAt[j]; ok {
					callback(TokenId{node, j})
				}
			}
		}
		prevVirtualIndex = virtualIndex

		field := structure.Field(i)

		if field.Kind() == reflect.Slice {
			length := field.Len()
			for i := 0; i < length; i++ {
				forEachTokenInStructure(field.Index(i), callback, node)
				if restoreListCommas && i != length - 1 && (isFirstList || !restoreListCommasInFirstListOnly) {
					callback(TokenId{node, nextVirtualIndex})
					nextVirtualIndex++
				}
			}
			isFirstList = false
		} else if field.Type().Name() == "Pos" {
			pos := field.Interface().(token.Pos)
			if pos.IsValid() {
				callback(TokenId{node, virtualIndex})
			}
		} else if _, ok := field.Interface().(ast.Node); ok {
			forEachTokenInStructure(field, callback, node)
		}

		if i == 0 {
			if funcDecl, ok := node.(*ast.FuncDecl); ok {
				callback(TokenId{funcDecl.Type, 0})
			}
		}

	}

}

var selectorExprVirtualFieldIndexMap   = []int{0, 2}
var sliceExprVirtualFieldIndexMap      = []int{0, 1, 2, 4, 6, 7, 8}
var typeAssertExprVirtualFieldIndexMap = []int{0, 2, 3, 4}
var arrayTypeVirtualFieldIndexMap      = []int{0, 1, 3}
var mapTypeVirtualFieldIndexMap        = []int{0, 2, 4}
var ifStmtVirtualFieldIndexMap         = []int{0, 1, 2, 3, 5}
var rangeStmtVirtualFieldIndexMap      = []int{0, 1, 3, 4, 5, 7, 8}
var valueSpecVirtualFieldIndexMap      = []int{0, 1, 2, 4, 5}

func getVirtualFieldIndexMap(node ast.Node) []int {

	switch node.(type) {
	case *ast.SelectorExpr:
		return selectorExprVirtualFieldIndexMap
	case *ast.SliceExpr:
		return sliceExprVirtualFieldIndexMap
	case *ast.TypeAssertExpr:
		return typeAssertExprVirtualFieldIndexMap
	case *ast.ArrayType:
		return arrayTypeVirtualFieldIndexMap
	case *ast.MapType:
		return mapTypeVirtualFieldIndexMap
	case *ast.IfStmt:
		return ifStmtVirtualFieldIndexMap
	case *ast.RangeStmt:
		return rangeStmtVirtualFieldIndexMap
	case *ast.ValueSpec:
		return valueSpecVirtualFieldIndexMap
	default:
		return nil
	}

}

func ForEachFieldShallow(node ast.Node, callback func (reflect.Value)) {

	structure := reflect.ValueOf(node)
	if node == nil || structure.IsNil() {
		return
	}

	structure = resolve(structure)
	for i := 0; i < structure.NumField(); i++ {
		callback(structure.Field(i))
	}

}

func ForEachExprShallow(node ast.Node, callback func (*ast.Expr)) {

	ForEachFieldShallow(node, func(field reflect.Value) {

		if field.Kind() == reflect.Slice {
			if exprs, ok := field.Interface().([]ast.Expr); ok {
				for i := range exprs {
					callback(&exprs[i])
				}
			}
		} else if expr, ok := field.Interface().(ast.Expr); ok && expr != nil {
			callback(&expr)
			field.Set(reflect.ValueOf(expr))
		}

	})

}

func resolve(value reflect.Value) reflect.Value {

	for value.Kind() == reflect.Interface || value.Kind() == reflect.Ptr {
		value = value.Elem()
	}
	return value

}
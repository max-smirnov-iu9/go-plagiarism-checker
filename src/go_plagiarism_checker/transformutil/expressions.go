package transformutil

import "go/ast"

func ParenthesizeIfBinary(expr ast.Expr) ast.Expr {

	if _, ok := expr.(*ast.BinaryExpr); ok {
		return &ast.ParenExpr{
			Lparen: -1,
			X:      expr,
			Rparen: -1,
		}
	}

	return expr

}

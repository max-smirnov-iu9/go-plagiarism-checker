package transformutil

import (
	"go/ast"
	"go/token"
)

func FindStmtListPtr(node ast.Node) *[]ast.Stmt {

	switch stmt := node.(type) {
	case *ast.BlockStmt:
		return &stmt.List
	case *ast.CaseClause:
		return &stmt.Body
	case *ast.CommClause:
		return &stmt.Body
	default:
		return nil
	}

}

func PrependToStmtList(node ast.Node, stmt ast.Stmt) {

	list := FindStmtListPtr(node)
	*list = append([]ast.Stmt{stmt}, *list...)

}

func DeclToVarDecl(decl ast.Decl) *ast.GenDecl {

	if genDecl, ok := decl.(*ast.GenDecl); ok && genDecl.Tok == token.VAR {
		return genDecl
	}

	return nil

}

func StmtToVarDecl(stmt ast.Stmt) *ast.GenDecl {

	if declStmt, ok := stmt.(*ast.DeclStmt); ok {
		return DeclToVarDecl(declStmt.Decl)
	}

	return nil

}

// Searches for variable declaration in list; tries to find a var-block with parentheses
func FindVarDeclsInStmtList(list []ast.Stmt) (decls []*ast.GenDecl, varBlockDecl *ast.GenDecl) {

	varBlockDecl = nil
	for _, stmt := range list {
		genDecl := StmtToVarDecl(stmt)
		if genDecl != nil {
			if varBlockDecl == nil && genDecl.Lparen != token.NoPos {
				varBlockDecl = genDecl
			}
			decls = append(decls, genDecl)
		}
	}
	return

}

// Same, but for Decl list
func FindVarDeclsInDeclList(list []ast.Decl) (decls []*ast.GenDecl, varBlockDecl *ast.GenDecl) {

	varBlockDecl = nil
	for _, decl := range list {
		genDecl := DeclToVarDecl(decl)
		if genDecl != nil {
			if varBlockDecl == nil && genDecl.Lparen != token.NoPos {
				varBlockDecl = genDecl
			}
			decls = append(decls, genDecl)
		}
	}
	return

}

func FindAssignStmts(list []ast.Stmt) (stmts []*ast.AssignStmt) {

	for _, node := range list {
		if assignStmt, ok := node.(*ast.AssignStmt); ok && assignStmt.Tok == token.DEFINE {
			stmts = append(stmts, assignStmt)
		}
	}
	return

}
package transformutil

import (
	"go/ast"
	"go/token"
	"reflect"
)

func compareNodeLists(list1, list2 interface{}) int {

	slice1 := reflect.ValueOf(list1)
	slice2 := reflect.ValueOf(list2)
	if slice1.Kind() != reflect.Slice || slice2.Kind() != reflect.Slice {
		panic("compareNodeLists: slices expected")
	}

	lenDiff := slice1.Len() - slice2.Len()
	if lenDiff != 0 {
		return lenDiff
	}

	for i := 0; i < slice1.Len(); i++ {
		result := CompareNodes(slice1.Index(i).Interface().(ast.Node), slice2.Index(i).Interface().(ast.Node))
		if result != 0 {
			return result
		}
	}
	return 0

}

type scores struct {
	score1 int
	score2 int
	current int
}

func newScores() *scores {
	return &scores{
		score1:  -1,
		score2:  -1,
		current: 0,
	}
}

func (s *scores) assignNext(ok1, ok2 bool) {
	if ok1 {
		s.score1 = s.current
	} else if ok2 {
		s.score2 = s.current
	}
	s.current++
}

func (s *scores) validate1() bool {
	return s.score1 >= 0
}
func (s *scores) validate2() bool {
	return s.score2 >= 0
}

func (s *scores) diff() int {
	return s.score1 - s.score2
}

func CompareNodes(node1, node2 ast.Node) int {

	isNil1 := node1 == nil || reflect.ValueOf(node1).IsNil()
	isNil2 := node2 == nil || reflect.ValueOf(node2).IsNil()
	if isNil1 && !isNil2 {
		return -1
	}
	if !isNil1 && isNil2 {
		return 1
	}
	if isNil1 && isNil2 {
		return 0
	}

	scores := newScores()

	switch node1 := node1.(type) {

	case ast.Stmt:

		node2, ok := node2.(ast.Stmt)
		if !ok {
			panic("Can't compare Stmt with non-Stmt")
		}

		_, ok1 := node1.(*ast.EmptyStmt)
		_, ok2 := node2.(*ast.EmptyStmt)
		if ok1 && ok2 {
			return 0
		}
		scores.assignNext(ok1, ok2)

		declStmt1, ok1 := node1.(*ast.DeclStmt)
		declStmt2, ok2 := node2.(*ast.DeclStmt)
		if ok1 && ok2 {
			return CompareNodes(declStmt1.Decl, declStmt2.Decl)
		}
		scores.assignNext(ok1, ok2)

		labeledStmt1, ok1 := node1.(*ast.LabeledStmt)
		labeledStmt2, ok2 := node2.(*ast.LabeledStmt)
		if ok1 && ok2 {
			return CompareNodes(labeledStmt1.Stmt, labeledStmt2.Stmt)
		}
		scores.assignNext(ok1, ok2)

		exprStmt1, ok1 := node1.(*ast.ExprStmt)
		exprStmt2, ok2 := node2.(*ast.ExprStmt)
		if ok1 && ok2 {
			return CompareNodes(exprStmt1.X, exprStmt2.X)
		}
		scores.assignNext(ok1, ok2)

		sendStmt1, ok1 := node1.(*ast.SendStmt)
		sendStmt2, ok2 := node2.(*ast.SendStmt)
		if ok1 && ok2 {
			chanResult := CompareNodes(sendStmt1.Chan, sendStmt2.Chan)
			if chanResult != 0 {
				return chanResult
			}
			return CompareNodes(sendStmt1.Value, sendStmt2.Value)
		}
		scores.assignNext(ok1, ok2)

		incDecStmt1, ok1 := node1.(*ast.IncDecStmt)
		incDecStmt2, ok2 := node2.(*ast.IncDecStmt)
		if ok1 && ok2 {
			return CompareNodes(incDecStmt1.X, incDecStmt2.X)
		}
		scores.assignNext(ok1, ok2)

		assignStmt1, ok1 := node1.(*ast.AssignStmt)
		assignStmt2, ok2 := node2.(*ast.AssignStmt)
		if ok1 && ok2 {
			lhsResult := compareNodeLists(assignStmt1.Lhs, assignStmt2.Lhs)
			if lhsResult != 0 {
				return lhsResult
			}
			diff := getAssignStmtTokScore(assignStmt1.Tok) - getAssignStmtTokScore(assignStmt2.Tok)
			if diff != 0 {
				return diff
			}
			return compareNodeLists(assignStmt1.Rhs, assignStmt2.Rhs)
		}
		scores.assignNext(ok1, ok2)

		goStmt1, ok1 := node1.(*ast.GoStmt)
		goStmt2, ok2 := node2.(*ast.GoStmt)
		if ok1 && ok2 {
			return CompareNodes(goStmt1.Call, goStmt2.Call)
		}
		scores.assignNext(ok1, ok2)

		deferStmt1, ok1 := node1.(*ast.DeferStmt)
		deferStmt2, ok2 := node2.(*ast.DeferStmt)
		if ok1 && ok2 {
			return CompareNodes(deferStmt1.Call, deferStmt2.Call)
		}
		scores.assignNext(ok1, ok2)

		branchStmt1, ok1 := node1.(*ast.BranchStmt)
		branchStmt2, ok2 := node2.(*ast.BranchStmt)
		if ok1 && ok2 {
			diff := getBranchTokenScore(branchStmt1.Tok) - getBranchTokenScore(branchStmt2.Tok)
			if diff != 0 {
				return diff
			}
			return CompareNodes(branchStmt1.Label, branchStmt2.Label)
		}
		scores.assignNext(ok1, ok2)

		blockStmt1, ok1 := node1.(*ast.BlockStmt)
		blockStmt2, ok2 := node2.(*ast.BlockStmt)
		if ok1 && ok2 {
			return compareNodeLists(blockStmt1.List, blockStmt2.List)
		}
		scores.assignNext(ok1, ok2)

		ifStmt1, ok1 := node1.(*ast.IfStmt)
		ifStmt2, ok2 := node2.(*ast.IfStmt)
		if ok1 && ok2 {
			initResult := CompareNodes(ifStmt1.Init, ifStmt2.Init)
			if initResult != 0 {
				return initResult
			}
			condResult := CompareNodes(ifStmt1.Cond, ifStmt2.Cond)
			if condResult != 0 {
				return condResult
			}
			bodyResult := CompareNodes(ifStmt1.Body, ifStmt2.Body)
			if bodyResult != 0 {
				return bodyResult
			}
			return CompareNodes(ifStmt1.Else, ifStmt2.Else)
		}
		scores.assignNext(ok1, ok2)

		caseClause1, ok1 := node1.(*ast.CaseClause)
		caseClause2, ok2 := node2.(*ast.CaseClause)
		if ok1 && ok2 {
			listResult := compareNodeLists(caseClause1.List, caseClause2.List)
			if listResult != 0 {
				return listResult
			}
			return compareNodeLists(caseClause1.Body, caseClause2.Body)
		}
		scores.assignNext(ok1, ok2)

		switchStmt1, ok1 := node1.(*ast.SwitchStmt)
		switchStmt2, ok2 := node2.(*ast.SwitchStmt)
		if ok1 && ok2 {
			initResult := CompareNodes(switchStmt1.Init, switchStmt2.Init)
			if initResult != 0 {
				return initResult
			}
			tagResult := CompareNodes(switchStmt1.Tag, switchStmt2.Tag)
			if tagResult != 0 {
				return tagResult
			}
			return CompareNodes(switchStmt1.Body, switchStmt2.Body)
		}
		scores.assignNext(ok1, ok2)

		typeSwitchStmt1, ok1 := node1.(*ast.TypeSwitchStmt)
		typeSwitchStmt2, ok2 := node2.(*ast.TypeSwitchStmt)
		if ok1 && ok2 {
			initResult := CompareNodes(typeSwitchStmt1.Init, typeSwitchStmt2.Init)
			if initResult != 0 {
				return initResult
			}
			assignResult := CompareNodes(typeSwitchStmt1.Assign, typeSwitchStmt2.Assign)
			if assignResult != 0 {
				return assignResult
			}
			return CompareNodes(typeSwitchStmt1.Body, typeSwitchStmt2.Body)
		}
		scores.assignNext(ok1, ok2)

		commClause1, ok1 := node1.(*ast.CommClause)
		commClause2, ok2 := node2.(*ast.CommClause)
		if ok1 && ok2 {
			commResult := CompareNodes(commClause1.Comm, commClause2.Comm)
			if commResult != 0 {
				return commResult
			}
			return compareNodeLists(commClause1.Body, commClause2.Body)
		}
		scores.assignNext(ok1, ok2)

		selectStmt1, ok1 := node1.(*ast.SelectStmt)
		selectStmt2, ok2 := node2.(*ast.SelectStmt)
		if ok1 && ok2 {
			return CompareNodes(selectStmt1.Body, selectStmt2.Body)
		}
		scores.assignNext(ok1, ok2)

		forStmt1, ok1 := node1.(*ast.ForStmt)
		forStmt2, ok2 := node2.(*ast.ForStmt)
		if ok1 && ok2 {
			initResult := CompareNodes(forStmt1.Init, forStmt2.Init)
			if initResult != 0 {
				return initResult
			}
			condResult := CompareNodes(forStmt1.Cond, forStmt2.Cond)
			if condResult != 0 {
				return condResult
			}
			postResult := CompareNodes(forStmt1.Post, forStmt2.Post)
			if postResult != 0 {
				return postResult
			}
			return CompareNodes(forStmt1.Body, forStmt2.Body)
		}
		scores.assignNext(ok1, ok2)

		rangeStmt1, ok1 := node1.(*ast.RangeStmt)
		rangeStmt2, ok2 := node2.(*ast.RangeStmt)
		if ok1 && ok2 {
			keyResult := CompareNodes(rangeStmt1.Key, rangeStmt2.Key)
			if keyResult != 0 {
				return keyResult
			}
			valueResult := CompareNodes(rangeStmt1.Value, rangeStmt2.Value)
			if valueResult != 0 {
				return valueResult
			}
			if rangeStmt1.Tok == token.ASSIGN && rangeStmt2.Tok == token.DEFINE {
				return -1
			} else if rangeStmt1.Tok == token.DEFINE && rangeStmt2.Tok == token.ASSIGN {
				return 1
			}
			exprResult := CompareNodes(rangeStmt1.X, rangeStmt2.X)
			if exprResult != 0 {
				return exprResult
			}
			return CompareNodes(rangeStmt1.Body, rangeStmt2.Body)
		}
		scores.assignNext(ok1, ok2)

		returnStmt1, ok1 := node1.(*ast.ReturnStmt)
		returnStmt2, ok2 := node2.(*ast.ReturnStmt)
		if ok1 && ok2 {
			return compareNodeLists(returnStmt1.Results, returnStmt2.Results)
		}
		scores.assignNext(ok1, ok2)

	case ast.Expr:

		node2, ok := node2.(ast.Expr)
		if !ok {
			panic("Can't compare Expr with non-Expr")
		}

		parenExpr1, ok1 := node1.(*ast.ParenExpr)
		parenExpr2, ok2 := node2.(*ast.ParenExpr)
		if ok1 && ok2 {
			return CompareNodes(parenExpr1.X, parenExpr2.X)
		}
		scores.assignNext(ok1, ok2)

		funcLit1, ok1 := node1.(*ast.FuncLit)
		funcLit2, ok2 := node2.(*ast.FuncLit)
		if ok1 && ok2 {
			typeResult := CompareNodes(funcLit1.Type, funcLit2.Type)
			if typeResult != 0 {
				return typeResult
			}
			return CompareNodes(funcLit1.Body, funcLit2.Body)
		}
		scores.assignNext(ok1, ok2)

		composeLit1, ok1 := node1.(*ast.CompositeLit)
		composeLit2, ok2 := node2.(*ast.CompositeLit)
		if ok1 && ok2 {
			typeResult := CompareNodes(composeLit1.Type, composeLit2.Type)
			if typeResult != 0 {
				return typeResult
			}
			return compareNodeLists(composeLit1.Elts, composeLit2.Elts)
		}
		scores.assignNext(ok1, ok2)

		selectorExpr1, ok1 := node1.(*ast.SelectorExpr)
		selectorExpr2, ok2 := node2.(*ast.SelectorExpr)
		if ok1 && ok2 {
			return CompareNodes(selectorExpr1.X, selectorExpr2.X)
		}
		scores.assignNext(ok1, ok2)

		indexExpr1, ok1 := node1.(*ast.IndexExpr)
		indexExpr2, ok2 := node2.(*ast.IndexExpr)
		if ok1 && ok2 {
			exprResult := CompareNodes(indexExpr1.X, indexExpr2.X)
			if exprResult != 0 {
				return exprResult
			}
			return CompareNodes(indexExpr1.Index, indexExpr2.Index)
		}
		scores.assignNext(ok1, ok2)

		sliceExpr1, ok1 := node1.(*ast.SliceExpr)
		sliceExpr2, ok2 := node2.(*ast.SliceExpr)
		if ok1 && ok2 {
			exprResult := CompareNodes(sliceExpr1.X, sliceExpr2.X)
			if exprResult != 0 {
				return exprResult
			}
			lowResult := CompareNodes(sliceExpr1.Low, sliceExpr2.Low)
			if lowResult != 0 {
				return lowResult
			}
			highResult := CompareNodes(sliceExpr1.High, sliceExpr2.High)
			if highResult != 0 {
				return highResult
			}
			return CompareNodes(sliceExpr1.Max, sliceExpr2.Max)
		}
		scores.assignNext(ok1, ok2)

		typeAssertExpr1, ok1 := node1.(*ast.TypeAssertExpr)
		typeAssertExpr2, ok2 := node2.(*ast.TypeAssertExpr)
		if ok1 && ok2 {
			exprResult := CompareNodes(typeAssertExpr1.X, typeAssertExpr2.X)
			if exprResult != 0 {
				return exprResult
			}
			return CompareNodes(typeAssertExpr1.Type, typeAssertExpr2.Type)
		}
		scores.assignNext(ok1, ok2)

		callExpr1, ok1 := node1.(*ast.CallExpr)
		callExpr2, ok2 := node2.(*ast.CallExpr)
		if ok1 && ok2 {
			funResult := CompareNodes(callExpr1.Fun, callExpr2.Fun)
			if funResult != 0 {
				return funResult
			}
			argsResult := compareNodeLists(callExpr1.Args, callExpr2.Args)
			if argsResult != 0 {
				return argsResult
			}
			if callExpr1.Ellipsis == token.NoPos && callExpr2.Ellipsis != token.NoPos {
				return -1
			}
			if callExpr1.Ellipsis != token.NoPos && callExpr2.Ellipsis == token.NoPos {
				return 1
			}
			return 0
		}
		scores.assignNext(ok1, ok2)

		ellipsis1, ok1 := node1.(*ast.Ellipsis)
		ellipsis2, ok2 := node2.(*ast.Ellipsis)
		if ok1 && ok2 {
			return CompareNodes(ellipsis1.Elt, ellipsis2.Elt)
		}
		scores.assignNext(ok1, ok2)

		binaryExpr1, ok1 := node1.(*ast.BinaryExpr)
		binaryExpr2, ok2 := node2.(*ast.BinaryExpr)
		if ok1 && ok2 {
			expr1Result := CompareNodes(binaryExpr1.X, binaryExpr2.X)
			if expr1Result != 0 {
				return expr1Result
			}
			diff := getBinaryExprOpScore(binaryExpr1.Op) - getBinaryExprOpScore(binaryExpr2.Op)
			if diff != 0 {
				return diff
			}
			return CompareNodes(binaryExpr1.Y, binaryExpr2.Y)
		}
		scores.assignNext(ok1, ok2)

		unaryExpr1, ok1 := node1.(*ast.UnaryExpr)
		unaryExpr2, ok2 := node2.(*ast.UnaryExpr)
		if ok1 && ok2 {
			diff := getUnaryExprOpScore(unaryExpr1.Op) - getUnaryExprOpScore(unaryExpr2.Op)
			if diff != 0 {
				return diff
			}
			return CompareNodes(unaryExpr1.X, unaryExpr2.X)
		}
		scores.assignNext(ok1, ok2)

		starExpr1, ok1 := node1.(*ast.StarExpr)
		starExpr2, ok2 := node2.(*ast.StarExpr)
		if ok1 && ok2 {
			return CompareNodes(starExpr1.X, starExpr2.X)
		}
		scores.assignNext(ok1, ok2)

		_, ok1 = node1.(*ast.Ident)
		_, ok2 = node2.(*ast.Ident)
		if ok1 && ok2 {
			return 0
		}
		scores.assignNext(ok1, ok2)

		basicLit1, ok1 := node1.(*ast.BasicLit)
		basicLit2, ok2 := node2.(*ast.BasicLit)
		if ok1 && ok2 {
			return getBasicLitKindScore(basicLit1.Kind) - getBasicLitKindScore(basicLit2.Kind)
		}
		scores.assignNext(ok1, ok2)

		keyValueExpr1, ok1 := node1.(*ast.KeyValueExpr)
		keyValueExpr2, ok2 := node2.(*ast.KeyValueExpr)
		if ok1 && ok2 {
			keyResult := CompareNodes(keyValueExpr1.Key, keyValueExpr2.Key)
			if keyResult != 0 {
				return keyResult
			}
			return CompareNodes(keyValueExpr1.Value, keyValueExpr2.Value)
		}
		scores.assignNext(ok1, ok2)

		arrayType1, ok1 := node1.(*ast.ArrayType)
		arrayType2, ok2 := node2.(*ast.ArrayType)
		if ok1 && ok2 {
			lenResult := CompareNodes(arrayType1.Len, arrayType2.Len)
			if lenResult != 0 {
				return lenResult
			}
			return CompareNodes(arrayType1.Elt, arrayType2.Elt)
		}
		scores.assignNext(ok1, ok2)

		structType1, ok1 := node1.(*ast.StructType)
		structType2, ok2 := node2.(*ast.StructType)
		if ok1 && ok2 {
			return CompareNodes(structType1.Fields, structType2.Fields)
		}
		scores.assignNext(ok1, ok2)

		funcType1, ok1 := node1.(*ast.FuncType)
		funcType2, ok2 := node2.(*ast.FuncType)
		if ok1 && ok2 {
			paramsResult := CompareNodes(funcType1.Params, funcType2.Params)
			if paramsResult != 0 {
				return paramsResult
			}
			return CompareNodes(funcType1.Results, funcType2.Results)
		}
		scores.assignNext(ok1, ok2)

		interfaceType1, ok1 := node1.(*ast.InterfaceType)
		interfaceType2, ok2 := node2.(*ast.InterfaceType)
		if ok1 && ok2 {
			return CompareNodes(interfaceType1.Methods, interfaceType2.Methods)
		}
		scores.assignNext(ok1, ok2)

		mapType1, ok1 := node1.(*ast.MapType)
		mapType2, ok2 := node2.(*ast.MapType)
		if ok1 && ok2 {
			keyResult := CompareNodes(mapType1.Key, mapType2.Key)
			if keyResult != 0 {
				return keyResult
			}
			return CompareNodes(mapType1.Value, mapType2.Value)
		}
		scores.assignNext(ok1, ok2)

		chanType1, ok1 := node1.(*ast.ChanType)
		chanType2, ok2 := node2.(*ast.ChanType)
		if ok1 && ok2 {
			return CompareNodes(chanType1.Value, chanType2.Value)
		}
		scores.assignNext(ok1, ok2)

	case ast.Decl:

		node2, ok := node2.(ast.Decl)
		if !ok {
			panic("Can't compare Decl with non-Decl")
		}

		genDecl1, ok1 := node1.(*ast.GenDecl)
		genDecl2, ok2 := node2.(*ast.GenDecl)
		if ok1 && ok2 {
			diff := getGenDeclTokScore(genDecl1.Tok) - getGenDeclTokScore(genDecl2.Tok)
			if diff != 0 {
				return diff
			}
			if genDecl1.Lparen == token.NoPos && genDecl2.Lparen != token.NoPos {
				return -1
			}
			if genDecl1.Lparen != token.NoPos && genDecl2.Lparen == token.NoPos {
				return 1
			}
			return compareNodeLists(genDecl1.Specs, genDecl2.Specs)
		}
		scores.assignNext(ok1, ok2)

		funcDecl1, ok1 := node1.(*ast.FuncDecl)
		funcDecl2, ok2 := node2.(*ast.FuncDecl)
		if ok1 && ok2 {
			recvResult := CompareNodes(funcDecl1.Recv, funcDecl2.Recv)
			if recvResult != 0 {
				return recvResult
			}
			typeResult := CompareNodes(funcDecl1.Type, funcDecl2.Type)
			if typeResult != 0 {
				return typeResult
			}
			return CompareNodes(funcDecl1.Body, funcDecl2.Body)
		}
		scores.assignNext(ok1, ok2)

	case ast.Spec:

		node2, ok := node2.(ast.Spec)
		if !ok {
			panic("Can't compare Spec with non-Spec")
		}

		_, ok1 := node1.(*ast.ImportSpec)
		_, ok2 := node2.(*ast.ImportSpec)
		if ok1 && ok2 {
			return 0
		}
		scores.assignNext(ok1, ok2)

		valueSpec1, ok1 := node1.(*ast.ValueSpec)
		valueSpec2, ok2 := node2.(*ast.ValueSpec)
		if ok1 && ok2 {
			namesResult := compareNodeLists(valueSpec1.Names, valueSpec2.Names)
			if namesResult != 0 {
				return namesResult
			}
			typeResult := CompareNodes(valueSpec1.Type, valueSpec2.Type)
			if typeResult != 0 {
				return typeResult
			}
			return compareNodeLists(valueSpec1.Values, valueSpec2.Values)
		}
		scores.assignNext(ok1, ok2)

		typeSpec1, ok1 := node1.(*ast.TypeSpec)
		typeSpec2, ok2 := node2.(*ast.TypeSpec)
		if ok1 && ok2 {
			nameResult := CompareNodes(typeSpec1.Name, typeSpec2.Name)
			if nameResult != 0 {
				return nameResult
			}
			return CompareNodes(typeSpec1.Type, typeSpec2.Type)
		}
		scores.assignNext(ok1, ok2)

	case *ast.FieldList:

		node2, ok := node2.(*ast.FieldList)
		if !ok {
			panic("Can't compare FieldList with non-FieldList")
		}

		return compareNodeLists(node1.List, node2.List)

	case *ast.Field:

		node2, ok := node2.(*ast.Field)
		if !ok {
			panic("Can't compare Field with non-Field")
		}

		namesResult := compareNodeLists(node1.Names, node2.Names)
		if namesResult != 0 {
			return namesResult
		}
		return CompareNodes(node1.Type, node2.Type)

	default:

		panic("Unknown node1 type")

	}

	if !scores.validate1() {
		panic("Unknown node1 type")
	}
	if !scores.validate2() {
		panic("Unknown node2 type")
	}
	return scores.diff()

}

var assignStmtTokToScore = map[token.Token]int{
	token.ASSIGN: 0,
	token.ADD_ASSIGN: 1,
	token.SUB_ASSIGN: 2,
	token.MUL_ASSIGN: 3,
	token.QUO_ASSIGN: 4,
	token.REM_ASSIGN: 5,
	token.AND_ASSIGN: 6,
	token.OR_ASSIGN: 7,
	token.XOR_ASSIGN: 8,
	token.AND_NOT_ASSIGN: 9,
	token.SHL_ASSIGN: 10,
	token.SHR_ASSIGN: 11,
	token.DEFINE: 12,
}

func getAssignStmtTokScore(tok token.Token) int {
	return assignStmtTokToScore[tok]
}

var branchTokenToScore = map[token.Token]int{
	token.BREAK: 0,
	token.CONTINUE: 1,
	token.GOTO: 2,
	token.FALLTHROUGH: 3,
}

func getBranchTokenScore(branchToken token.Token) int {
	return branchTokenToScore[branchToken]
}

var basicLitKindToScore = map[token.Token]int{
	token.INT: 0,
	token.FLOAT: 1,
	token.IMAG: 2,
	token.CHAR: 3,
	token.STRING: 3,
}

func getBasicLitKindScore(kind token.Token) int {
	return basicLitKindToScore[kind]
}

var unaryExprOpToScore = map[token.Token]int{
	token.ADD: 0,
	token.SUB: 1,
	token.NOT: 2,
	token.XOR: 3,
	token.AND: 4,
	token.ARROW: 5,
}

func getUnaryExprOpScore(op token.Token) int {
	return unaryExprOpToScore[op]
}

var binaryExprOpToScore = map[token.Token]int{
	token.ADD: 0,
	token.SUB: 1,
	token.MUL: 2,
	token.QUO: 3,
	token.REM: 4,
	token.AND: 5,
	token.OR: 6,
	token.XOR: 7,
	token.AND_NOT: 8,
	token.SHL: 9,
	token.SHR: 10,
	token.EQL: 11,
	token.NEQ: 12,
	token.LSS: 13,
	token.LEQ: 14,
	token.GTR: 15,
	token.GEQ: 16,
	token.LAND: 17,
	token.LOR: 18,
}

func getBinaryExprOpScore(op token.Token) int {
	return binaryExprOpToScore[op]
}

var genDeclTokToScore = map[token.Token]int{
	token.IMPORT: 0,
	token.CONST: 1,
	token.TYPE: 2,
	token.VAR: 3,
}

func getGenDeclTokScore(tok token.Token) int {
	return genDeclTokToScore[tok]
}
package transform

import (
	"go/ast"
	"go/token"
	"go_plagiarism_checker/transformutil"
)

type varDeclSpecCollector struct {
	specs []ast.Spec
	varBlockDecl *ast.GenDecl
	varDecl *ast.GenDecl
}

func (c *varDeclSpecCollector) add(specs []ast.Spec, varBlockDecl *ast.GenDecl, existingVarDecls []*ast.GenDecl) {

	c.specs = append(c.specs, specs...)

	if c.varBlockDecl == nil {
		if varBlockDecl != nil {
			c.varBlockDecl = varBlockDecl
		} else if c.varDecl != nil && len(existingVarDecls) > 0 {
			c.varDecl = existingVarDecls[0]
		}
	}

}

func (c *varDeclSpecCollector) getVarBlockDecl() *ast.GenDecl {

	if len(c.specs) == 0 {
		return nil
	}

	if c.varBlockDecl == nil && c.varDecl == nil {
		// No existing var blocks or var declarations; creating a new one
		c.varBlockDecl = &ast.GenDecl{
			TokPos: -1,
			Tok:    token.VAR,
			Lparen: -1,
			Specs:  nil,
			Rparen: -1,
		}
	} else if c.varBlockDecl == nil {
		// No var declarations with parentheses
		c.varBlockDecl = c.varDecl
		c.varBlockDecl.Lparen = -1
		c.varBlockDecl.Rparen = -1
	}

	c.varBlockDecl.Specs = c.specs
	sortSpecList(c.varBlockDecl.Specs)

	return c.varBlockDecl

}

func (c *varDeclSpecCollector) extractVarDeclSpecsFromStmtList(stmtListPtr *[]ast.Stmt) {

	existingVarDecls, varBlockDecl := transformutil.FindVarDeclsInStmtList(*stmtListPtr)
	existingAssignStmts := transformutil.FindAssignStmts(*stmtListPtr)
	replacementAssignStmts := make(map[*ast.GenDecl][]*ast.AssignStmt)
	c.add(extractVarDeclSpecs(existingVarDecls, existingAssignStmts, replacementAssignStmts), varBlockDecl, existingVarDecls)

	// Replacing VarDecls with assign stmts
	if len(existingVarDecls) > 0 {
		newStmtList := make([]ast.Stmt, 0, len(*stmtListPtr) - len(existingVarDecls) + 1)
		for _, stmt := range *stmtListPtr {
			varDecl := transformutil.StmtToVarDecl(stmt)
			if varDecl == nil {
				newStmtList = append(newStmtList, stmt)
			} else {
				for _, assignStmt := range replacementAssignStmts[varDecl] {
					newStmtList = append(newStmtList, assignStmt)
				}
			}
		}
		*stmtListPtr = newStmtList
	}

}

func (c *varDeclSpecCollector) extractVarDeclSpecsFromDeclList(declListPtr *[]ast.Decl) {

	existingVarDecls, varBlockDecl := transformutil.FindVarDeclsInDeclList(*declListPtr)
	c.add(extractVarDeclSpecs(existingVarDecls, nil, nil), varBlockDecl, existingVarDecls)

	// Deleting VarDecls from the list
	if len(existingVarDecls) > 0 {
		newDeclList := make([]ast.Decl, 0, len(*declListPtr) - len(existingVarDecls) + 1)
		for _, decl := range *declListPtr {
			varDecl := transformutil.DeclToVarDecl(decl)
			if varDecl == nil {
				newDeclList = append(newDeclList, decl)
			}
		}
		*declListPtr = newDeclList
	}

}

func extractVarDeclSpecs(existingVarDecls []*ast.GenDecl, existingAssignStmts []*ast.AssignStmt,
	replacementAssignStmts map[*ast.GenDecl][]*ast.AssignStmt) (varDeclSpecs []ast.Spec) {

	if len(existingVarDecls) == 0 && len(existingAssignStmts) == 0 {
		return
	}

	for _, assignStmt := range existingAssignStmts {
		for i, lhs := range assignStmt.Lhs {
			if ident, ok := lhs.(*ast.Ident); ok {
				if ident.Name == "_" {
					continue
				}
				var rhsType ast.Expr
				if len(assignStmt.Rhs) == len(assignStmt.Lhs) {
					rhsType = inferType(assignStmt.Rhs[i])
				} else {
					rhsType = &ast.Ident{
						NamePos: -1,
						Name:    "int",
						Obj:     nil,
					}
				}
				spec := &ast.ValueSpec{
					Names: []*ast.Ident{{
						NamePos: -1,
						Name:    ident.Name,
						Obj:     nil,
					}},
					Type: rhsType,
					Values: []ast.Expr{getDefaultValueByType(rhsType)},
				}
				varDeclSpecs = append(varDeclSpecs, spec)
			}
		}
		// Replacing ':=' with '='
		assignStmt.Tok = token.ASSIGN
	}

	for _, varDecl := range existingVarDecls {
		if varDecl.Specs != nil {
			for i := 0; i < len(varDecl.Specs); i++ {
				spec := varDecl.Specs[i].(*ast.ValueSpec)
				// Breaking up multiple comma-separated variables into separate Specs
				if len(spec.Names) > 1 {
					for j := len(spec.Names) - 1; j >= 1; j-- {
						// There's either one type and no values, or as many values as variables
						newType := spec.Type
						newValues := []ast.Expr{getDefaultValueByType(newType)}
						if len(spec.Values) > 1 {
							newType = inferType(spec.Values[j])
							newValues = spec.Values[j:j + 1]
						}
						newSpec := &ast.ValueSpec{
							Names:   []*ast.Ident{spec.Names[j]},
							Type:    newType,
							Values:  newValues,
						}
						varDecl.Specs = append(varDecl.Specs[:i + 1],
							append([]ast.Spec{newSpec}, varDecl.Specs[i + 1:]...)...)
					}
					spec.Names = spec.Names[:1]
					if len(spec.Values) > 1 {
						spec.Values = spec.Values[:1]
					}
					i--
					continue
				}
				// Adding a default type
				if spec.Type == nil {
					spec.Type = inferType(spec.Values[0])
				}
				// Storing a replacement assign stmt
				newValues := []ast.Expr{getDefaultValueByType(spec.Type)}
				oldValues := newValues
				if len(spec.Values) > 0 {
					oldValues = spec.Values
				}
				if replacementAssignStmts != nil {
					replacementAssignStmts[varDecl] = append(replacementAssignStmts[varDecl], &ast.AssignStmt{
						Lhs:    []ast.Expr{spec.Names[0]},
						TokPos: -1,
						Tok:    token.ASSIGN,
						Rhs:    oldValues,
					})
				}
				// Setting a default value
				spec.Values = newValues
			}
		}
		varDeclSpecs = append(varDeclSpecs, varDecl.Specs...)
	}

	return

}

func inferType(expr ast.Expr) ast.Expr {

	identName := "int"
	switch expr := expr.(type) {
	case *ast.BasicLit:
		switch expr.Kind {
		case token.FLOAT:
			identName = "float32"
		case token.IMAG:
			identName = "complex64"
		case token.STRING:
			identName = "string"
		}
	case *ast.CallExpr:
		if ident, ok := expr.Fun.(*ast.Ident); ok && ident.Name == "make" && len(expr.Args) > 0 {
			return expr.Args[0]
		}
	case *ast.Ident:
		if expr.Name == "true" || expr.Name == "false" {
			identName = "bool"
		}
	}

	return &ast.Ident{
		NamePos: -1,
		Name:    identName,
		Obj:     nil,
	}

}

func getDefaultValueByType(expr ast.Expr) ast.Expr {

	if ident, ok := expr.(*ast.Ident); ok {
		if ident.Name == "bool" {
			return &ast.Ident{
				NamePos: -1,
				Name:    "false",
				Obj:     nil,
			}
		}
		kind := token.ILLEGAL
		var value string
		switch ident.Name {
		case "int", "uint", "int8", "int16", "int32", "int64", "uint8", "uint16", "uint32", "uint64", "rune", "byte", "uintptr", "complex64", "complex128":
			kind = token.INT
			value = "0"
		case "float32", "float64":
			kind = token.FLOAT
			value = "0.0"
		case "string":
			kind = token.STRING
			value = "\"\""
		}
		if kind != token.ILLEGAL {
			return &ast.BasicLit{
				ValuePos: -1,
				Kind:     kind,
				Value:    value,
			}
		}
	}

	return &ast.Ident{
		NamePos: -1,
		Name:    "nil",
		Obj:     nil,
	}

}
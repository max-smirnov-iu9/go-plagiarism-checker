package transform

import (
	"go/ast"
	"go/token"
	"go_plagiarism_checker/transformutil"
	"golang.org/x/tools/go/ast/astutil"
)

func (t *Transformer) extractForStmtHeader(cursor *astutil.Cursor, node *ast.ForStmt) {

	if cursor.Index() < 0 {
		return
	}

	if node.Init != nil {
		cursor.InsertBefore(node.Init)
		node.Init = nil
	}

	if node.Cond != nil {
		transformutil.PrependToStmtList(node.Body, &ast.IfStmt{
			If: -1,
			Cond: t.negateCond(node.Cond),
			Body: &ast.BlockStmt{
				Lbrace: -1,
				List: []ast.Stmt{
					&ast.BranchStmt{
						TokPos: -1,
						Tok: token.BREAK,
					},
				},
				Rbrace: -1,
			},
		})
		node.Cond = nil
	}
	
	if node.Post != nil {
		node.Body.List = append(node.Body.List, node.Post)
		node.Post = nil
		t.transformAssignStmts(&node.Body.List)
	}

}

func isIfElseEmpty(node *ast.IfStmt) bool {
	elseBlock, isElseABlock := node.Else.(*ast.BlockStmt)
	return len(node.Body.List) == 0 && (node.Else == nil || isElseABlock && len(elseBlock.List) == 0)
}

func (t *Transformer) transformIfElse(cursor *astutil.Cursor, node *ast.IfStmt) {

	elseBlock, isElseABlock := node.Else.(*ast.BlockStmt)

	// Deleting the stmt if both if and else blocks are empty
	if cursor.Index() >= 0 && isIfElseEmpty(node) {
		cursor.Delete()
		return
	}

	// Extracting init statement
	if cursor.Index() >= 0 && node.Init != nil && cursor.Index() >= 0 {
		cursor.InsertBefore(node.Init)
		node.Init = nil
	}

	// If the else branch has an empty block, deleting it
	if node.Else != nil && (isElseABlock && len(elseBlock.List) == 0 || !isElseABlock && isIfElseEmpty(node.Else.(*ast.IfStmt))) {
		node.Else = nil
	}

	// Wrapping 'else if' in a block
	if node.Else != nil && !isElseABlock {
		elseBlock = &ast.BlockStmt{
			Lbrace: -1,
			List:   []ast.Stmt{node.Else},
			Rbrace: -1,
		}
		node.Else = elseBlock
	}

	// If the 'if' branch is empty, but not the 'else' branch, swapping them
	negateCond := false
	if len(node.Body.List) == 0 {
		node.Body = elseBlock
		node.Else = nil
		negateCond = true
	}

	// If the 'if' branch is bigger than the 'else' branch, swapping them
	if node.Else != nil && transformutil.CompareNodes(node.Body, node.Else) > 0 {
		node.Body, node.Else = elseBlock, node.Body
		negateCond = !negateCond
	}

	// If the swap took place, negating the condition
	if negateCond {
		node.Cond = t.negateCond(node.Cond)
	}

}

func (t *Transformer) negateCond(cond ast.Expr) ast.Expr {

	newCond := ast.Expr(&ast.UnaryExpr{
		OpPos: -1,
		Op: token.NOT,
		X: &ast.ParenExpr{
			Lparen: -1,
			X:      cond,
			Rparen: -1,
		},
	})

	if t.EnabledTransforms & Exprs != 0 {
		t.transformRootExpr(&newCond)
	}

	return cond

}
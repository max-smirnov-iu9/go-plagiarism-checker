package transform

import (
	"bufio"
	"fmt"
	"go/ast"
	"go/parser"
	"go/printer"
	"go/token"
	"go_plagiarism_checker/transformutil"
	"golang.org/x/tools/go/ast/astutil"
	"io"
	"log"
	"os"
)

type EnabledTransforms uint
const (
	VarBlocks EnabledTransforms = 1 << iota
	AssignStmts
	Sort
	Exprs
	ControlStmts
)

type Transformer struct {
	EnabledTransforms EnabledTransforms
	fileSet *token.FileSet
	Tokens  []Token
	Lines   []string
	Tree    *ast.File

	tokenIdToIndex map[transformutil.TokenId]int
	replacedTokens map[transformutil.TokenId]transformutil.TokenId
	TokenMap       map[int]int
}

func (t *Transformer) setTokenReplaced(newId, oldId transformutil.TokenId) {

	if actualOldId, ok := t.replacedTokens[oldId]; ok {
		t.replacedTokens[newId] = actualOldId
		delete(t.replacedTokens, oldId)
	} else {
		t.replacedTokens[newId] = oldId
	}

}

func (t *Transformer) Init(sourcePath string) {

	var src []byte

	srcFile, err := os.Open(sourcePath)
	if err != nil {
		panic(fmt.Sprintf("os.Open error: %v", err))
	}
	defer srcFile.Close()

	scanner := bufio.NewScanner(srcFile)
	for scanner.Scan() {
		t.Lines = append(t.Lines, scanner.Text())
		src = append(src, scanner.Bytes()...)
		src = append(src, '\n')
	}

	fileSet := token.NewFileSet()
	file := fileSet.AddFile(sourcePath, fileSet.Base(), len(src))

	t.Tokens = TokenizeSource(file, src)

	t.fileSet = token.NewFileSet()
	tree, err := parser.ParseFile(t.fileSet, sourcePath, src, parser.AllErrors)
	if err != nil {
		log.Fatal(err)
	}
	t.Tree = tree
	//spew.Dump(tree)

	t.tokenIdToIndex = make(map[transformutil.TokenId]int)
	t.replacedTokens = make(map[transformutil.TokenId]transformutil.TokenId)
	i := 0
	transformutil.ForEachToken(tree, func(tokenId transformutil.TokenId) {
		t.tokenIdToIndex[tokenId] = i
		i++
	})

}

func (t *Transformer) Transform() {

	varDeclSpecCollector := &varDeclSpecCollector{}
	if t.EnabledTransforms & VarBlocks != 0 {
		varDeclSpecCollector.extractVarDeclSpecsFromDeclList(&t.Tree.Decls)
	}

	astutil.Apply(t.Tree, nil, func(cursor *astutil.Cursor) bool {

		node := cursor.Node()

		switch node := node.(type) {

		case *ast.ForStmt:
			if t.EnabledTransforms & ControlStmts != 0 {
				t.extractForStmtHeader(cursor, node)
			}

		case *ast.IfStmt:
			if t.EnabledTransforms & ControlStmts != 0 {
				t.transformIfElse(cursor, node)
			}

		default:
			stmtListPtr := transformutil.FindStmtListPtr(node)
			if stmtListPtr != nil {
				if t.EnabledTransforms & VarBlocks != 0 {
					varDeclSpecCollector.extractVarDeclSpecsFromStmtList(stmtListPtr)
				}
				if t.EnabledTransforms & AssignStmts != 0 {
					t.transformAssignStmts(stmtListPtr)
				}
				if t.EnabledTransforms & Sort != 0 {
					sortStmtList(*stmtListPtr)
				}
			}

		}

		if t.EnabledTransforms & Exprs != 0 {
			t.findAndTransformRootExprs(node)
		}

		return true

	})

	if t.EnabledTransforms & VarBlocks != 0 {
		varBlockDecl := varDeclSpecCollector.getVarBlockDecl()
		if varBlockDecl != nil {
			t.Tree.Decls = append(t.Tree.Decls, varBlockDecl)
		}
	}

	if t.EnabledTransforms & Sort != 0 {
		sortDeclList(t.Tree.Decls)
	}

	//spew.Dump(t.Tree)

	t.TokenMap = make(map[int]int)
	to := 0
	transformutil.ForEachToken(t.Tree, func(tokenId transformutil.TokenId) {
		ok := t.addTokenIndexPair(tokenId, to)
		if !ok {
			if replacement, ok := t.replacedTokens[tokenId]; ok {
				t.addTokenIndexPair(replacement, to)
			}
		}
		to++
	})

	//_ = printer.Fprint(os.Stdout, t.fileSet, t.Tree)

	t.Tokens = TokenizeTree(t.fileSet, t.Tree)

	writer := &LineWriter{}
	writer.init()
	_ = printer.Fprint(writer, t.fileSet, t.Tree)
	t.Lines = writer.lines

	//spew.Dump(t.tokenMap)

}

func (t *Transformer) addTokenIndexPair(tokenId transformutil.TokenId, to int) bool {

	from, ok := t.tokenIdToIndex[tokenId]
	if ok {
		t.TokenMap[to] = from
	}
	return ok

}

type LineWriter struct {
	io.Writer
	lines []string
}

func (lw *LineWriter) init() {
	lw.lines = []string{""}
}

func (lw *LineWriter) Write(p []byte) (n int, err error) {

	line := &lw.lines[len(lw.lines) - 1]
	for _, char := range p {
		if char == '\n' {
			lw.lines = append(lw.lines, "")
			line = &lw.lines[len(lw.lines) - 1]
		} else {
			*line += string(char)
		}
	}

	return len(p), nil

}
package transform

import (
	"go/ast"
	"go/token"
	"go_plagiarism_checker/transformutil"
	"reflect"
	"strconv"
)

func (t *Transformer) findAndTransformRootExprs(node ast.Node) {

	// We may assume that expressions only contain other expressions as subnodes
	// The only exception to this seems to be a function literal
	if _, ok := node.(ast.Expr); ok {
		return
	}

	transformutil.ForEachExprShallow(node, t.transformRootExpr)

}

func (t *Transformer) transformRootExpr(exprPtr *ast.Expr) {

	expr := *exprPtr

	// Skipping type expressions
	switch expr.(type) {
	case *ast.ArrayType:
		return
	case *ast.StructType:
		return
	case *ast.FuncType:
		return
	case *ast.InterfaceType:
		return
	case *ast.MapType:
		return
	case *ast.ChanType:
		return
	}

	t.transformSubrootExpr(exprPtr)
	*exprPtr, _ = stripParentheses(*exprPtr)

}

func (t *Transformer) transformSubrootExpr(exprPtr *ast.Expr) {

	expr := *exprPtr
	if expr == nil || reflect.ValueOf(expr).IsNil() {
		return
	}

	// `exprValue`s cannot be transformed further
	exprValue, _ := exprToExprValue(expr)
	if exprValue != nil {
		return
	}

	// Stripping unnecessary parentheses based on operation precedence
	transformutil.ForEachExprShallow(expr, func(exprPtr *ast.Expr) {
		t.transformSubrootExpr(exprPtr)
		if innerParenExpr, ok := (*exprPtr).(*ast.ParenExpr); ok {
			innerPrecedence, outerPrecedence := getOperationPrecedence(innerParenExpr.X), getOperationPrecedence(expr)
			if innerPrecedence <= outerPrecedence {
				strip := true
				outerBinaryExpr, ok := expr.(*ast.BinaryExpr)
				if innerPrecedence == outerPrecedence && ok {
					if innerParenExpr == outerBinaryExpr.X && !isBinaryOperationLeftAssoc(outerBinaryExpr.Op) {
						strip = false
					}
					if innerParenExpr == outerBinaryExpr.Y && !isBinaryOperationRightAssoc(outerBinaryExpr.Op) {
						strip = false
					}
				}
				if strip {
					*exprPtr, _ = stripParentheses(*exprPtr)
				}
			}
		}
	})

	// Evaluating constant expression
	switch expr := expr.(type) {
	case *ast.UnaryExpr:

		exprValue, _ := exprToExprValue(expr.X)
		if exprValue == nil {
			break
		}

		switch expr.Op {
		case token.NOT:
			boolExprValue := exprValue.(*boolExprValue)
			boolExprValue.value = !boolExprValue.value
			*exprPtr, _ = exprValueToExpr(boolExprValue)
		}

		return

	case *ast.BinaryExpr:

		exprValueX, _ := exprToExprValue(expr.X)
		exprValueY, _ := exprToExprValue(expr.Y)
		if exprValueX == nil || exprValueY == nil {
			if zero, ok := intsBinaryOperationToZero[expr.Op]; ok {
				if exprValueX != nil && exprValueX.(*intExprValue).value == zero && isBinaryOperationLeftAssoc(expr.Op) ||
					exprValueY != nil && exprValueY.(*intExprValue).value == zero && isBinaryOperationRightAssoc(expr.Op) {
					*exprPtr, _ = exprValueToExpr(&intExprValue{value: zero})
					return
				} else if expr.Op == token.AND_NOT && exprValueY != nil && exprValueY.(*intExprValue).value == -1 {
					*exprPtr, _ = exprValueToExpr(&intExprValue{value: 0})
					return
				}
			} else if zero, ok := boolsBinaryOperationToZero[expr.Op]; ok {
				if exprValueX != nil && exprValueX.(*boolExprValue).value == zero && isBinaryOperationLeftAssoc(expr.Op) ||
					exprValueY != nil && exprValueY.(*boolExprValue).value == zero && isBinaryOperationRightAssoc(expr.Op) {
					*exprPtr, _ = exprValueToExpr(&boolExprValue{value: zero})
					return
				}
			}
			if identity, ok := intsBinaryOperationToIdentity[expr.Op]; ok {
				if exprValueX != nil && exprValueX.(*intExprValue).value == identity && isBinaryOperationRightAssoc(expr.Op) {
					*exprPtr = expr.Y
					return
				} else if exprValueY != nil && exprValueY.(*intExprValue).value == identity && isBinaryOperationLeftAssoc(expr.Op) {
					*exprPtr = expr.X
					return
				} else if expr.Op == token.AND_NOT && exprValueX != nil && exprValueX.(*intExprValue).value == -1 {
					*exprPtr = &ast.UnaryExpr{
						OpPos: -1,
						Op:    token.XOR,
						X:     expr.Y,
					}
					return
				}
			} else if identity, ok := boolsBinaryOperationToIdentity[expr.Op]; ok {
				if exprValueX != nil && exprValueX.(*boolExprValue).value == identity && isBinaryOperationRightAssoc(expr.Op) {
					*exprPtr = expr.Y
					return
				} else if exprValueY != nil && exprValueY.(*boolExprValue).value == identity && isBinaryOperationLeftAssoc(expr.Op) {
					*exprPtr = expr.X
					return
				}
			}
			break
		}

		switch binaryOperationToOperandType[expr.Op] {
		case ints:
			intValue := intsBinaryOperationReduce[expr.Op](
				exprValueX.(*intExprValue).value,
				exprValueY.(*intExprValue).value)
			*exprPtr, _ = exprValueToExpr(&intExprValue{value: intValue})
		case bools:
			boolValue := boolsBinaryOperationReduce[expr.Op](
				exprValueX.(*boolExprValue).value,
				exprValueY.(*boolExprValue).value)
			*exprPtr, _ = exprValueToExpr(&boolExprValue{value: boolValue})
		case intsToBool:
			boolValue := intsToBoolBinaryOperationReduce[expr.Op](
				exprValueX.(*intExprValue).value,
				exprValueY.(*intExprValue).value)
			*exprPtr, _ = exprValueToExpr(&boolExprValue{value: boolValue})
		case intsOrBoolsToBool:
			if _, ok := exprValueX.(*intExprValue); ok {
				boolValue := intsToBoolBinaryOperationReduce[expr.Op](
					exprValueX.(*intExprValue).value,
					exprValueY.(*intExprValue).value)
				*exprPtr, _ = exprValueToExpr(&boolExprValue{value: boolValue})
			} else {
				boolValue := boolsBinaryOperationReduce[expr.Op](
					exprValueX.(*boolExprValue).value,
					exprValueY.(*boolExprValue).value)
				*exprPtr, _ = exprValueToExpr(&boolExprValue{value: boolValue})
			}
		}

		return

	}

	// The following expressions can be transformed further
	switch expr := expr.(type) {

	case *ast.UnaryExpr:

		switch expr.Op {
		case token.ADD: // Unary '+' does nothing
			*exprPtr = expr.X
		case token.SUB: // Unary '-'
			*exprPtr = t.arithmNegateExpr(expr.X, &transformutil.TokenId{expr, 0})
		case token.NOT: // Unary '!'
			*exprPtr = t.boolNegateExpr(expr.X, &transformutil.TokenId{expr, 0})
		}
		if unaryExpr, ok := (*exprPtr).(*ast.UnaryExpr); !ok || unaryExpr.Op != expr.Op {
			t.transformSubrootExpr(exprPtr)
		}

	case *ast.BinaryExpr:

		// Sorting operands of a commutative operation
		if isBinaryOperationCommutative(expr.Op) {
			operands, opTokenIds := getCommutativeBinaryOperands(expr, expr.Op)
			// Reducing constants
			if isBinaryOperationLeftAssoc(expr.Op) && isBinaryOperationRightAssoc(expr.Op) &&
				binaryOperationToOperandType[expr.Op] == ints {
				operands = t.reduceInts(operands, intsBinaryOperationReduce[expr.Op],
					intsBinaryOperationToIdentity[expr.Op])
			}
			sortExprList(operands)
			*exprPtr = t.combineToBinaryExpr(operands, opTokenIds, expr.Op)
		}

		// Replacing specific operators
		switch expr.Op {
		case token.SUB:
			// Replacing '-' with '+'
			expr.Op = token.ADD
			expr.Y = t.arithmNegateExpr(expr.Y, nil)
			t.transformSubrootExpr(exprPtr)
		case token.EQL, token.NEQ:
			// Removing "== true", "== false", "!= true", "!= false"
			if identX, ok := expr.X.(*ast.Ident); ok && (identX.Name == "true" || identX.Name == "false") {
				expr.X, expr.Y = expr.Y, expr.X
			}
			if identY, ok := expr.Y.(*ast.Ident); ok && (identY.Name == "true" || identY.Name == "false") {
				if (expr.Op == token.EQL) == (identY.Name == "false") {
					*exprPtr = t.boolNegateExpr(expr.X, &transformutil.TokenId{expr, 1})
					t.transformSubrootExpr(exprPtr)
				} else {
					*exprPtr = expr.X
				}
			}
		case token.GTR, token.LEQ, token.GEQ:
			// Replacing '<=', '>=', '>' with '<'
			if expr.Op == token.GTR || expr.Op == token.GEQ {
				expr.X, expr.Y = expr.Y, expr.X
			}
			basicLitY, isBasicLit := expr.Y.(*ast.BasicLit)
			if !isBasicLit || basicLitY.Kind != token.INT || basicLitY.Value != "0" {
				expr.X = &ast.BinaryExpr{
					X:     expr.X,
					OpPos: -1,
					Op:    token.SUB,
					Y:     expr.Y,
				}
				expr.Y = &ast.BasicLit{
					ValuePos: -1,
					Kind:     token.INT,
					Value:    "0",
				}
			}
			if expr.Op == token.LEQ || expr.Op == token.GEQ {
				expr.X = &ast.BinaryExpr{
					X:     expr.X,
					OpPos: -1,
					Op:    token.SUB,
					Y: &ast.BasicLit{
						ValuePos: -1,
						Kind:     token.INT,
						Value:    "1",
					},
				}
			}
			expr.Op = token.LSS
			t.transformSubrootExpr(exprPtr)
		}

	}

}

func (t *Transformer) arithmNegateExpr(expr ast.Expr, subTokenId *transformutil.TokenId) ast.Expr {

	expr, parenExpr := stripParentheses(expr)

	switch knownExpr := expr.(type) {

	case *ast.UnaryExpr:
		switch knownExpr.Op {
		case token.ADD:
			knownExpr.Op = token.SUB
			return knownExpr
		case token.SUB:
			return knownExpr.X
		}

	case *ast.BinaryExpr:
		switch knownExpr.Op {
		case token.ADD:
			knownExpr.X = t.arithmNegateExpr(knownExpr.X, subTokenId)
			knownExpr.Y = t.arithmNegateExpr(knownExpr.Y, subTokenId)
			return knownExpr
		case token.SUB:
			knownExpr.X = t.arithmNegateExpr(knownExpr.X, subTokenId)
			knownExpr.Op = token.ADD
			return knownExpr
		case token.MUL, token.QUO:
			knownExpr.X = t.arithmNegateExpr(knownExpr.X, subTokenId)
			return knownExpr
		default:
			expr = parenthesize(knownExpr, parenExpr)
		}

	}

	unaryExpr := &ast.UnaryExpr{
		OpPos: -1,
		Op:    token.SUB,
		X:     expr,
	}
	if subTokenId != nil {
		t.setTokenReplaced(transformutil.TokenId{unaryExpr, 0}, *subTokenId)
	}
	return unaryExpr

}

func (t *Transformer) boolNegateExpr(expr ast.Expr, notTokenId *transformutil.TokenId) ast.Expr {

	expr, parenExpr := stripParentheses(expr)

	switch knownExpr := expr.(type) {

	case *ast.UnaryExpr:
		switch knownExpr.Op {
		case token.NOT:
			return knownExpr.X
		}

	case *ast.BinaryExpr:
		switch knownExpr.Op {
		case token.LOR:
			knownExpr.X = t.boolNegateExpr(knownExpr.X, notTokenId)
			if binaryExpr, ok := knownExpr.X.(*ast.BinaryExpr); ok && binaryExpr.Op == token.LOR {
				knownExpr.X = parenthesize(knownExpr.X, parenExpr)
				if knownExpr.X == parenExpr {
					parenExpr = nil
				}
			}
			knownExpr.Op = token.LAND
			knownExpr.Y = t.boolNegateExpr(knownExpr.Y, notTokenId)
			if binaryExpr, ok := knownExpr.Y.(*ast.BinaryExpr); ok && binaryExpr.Op == token.LOR {
				knownExpr.Y = parenthesize(knownExpr.Y, parenExpr)
			}
			return knownExpr
		case token.LAND:
			knownExpr.X = t.boolNegateExpr(knownExpr.X, notTokenId)
			knownExpr.Op = token.LOR
			knownExpr.Y = t.boolNegateExpr(knownExpr.Y, notTokenId)
			return knownExpr
		case token.EQL:
			knownExpr.Op = token.NEQ
			return knownExpr
		case token.NEQ:
			knownExpr.Op = token.EQL
			return knownExpr
		case token.LSS:
			knownExpr.Op = token.GEQ
			return knownExpr
		case token.GTR:
			knownExpr.Op = token.LEQ
			return knownExpr
		case token.LEQ:
			knownExpr.Op = token.GTR
			return knownExpr
		case token.GEQ:
			knownExpr.Op = token.LSS
			return knownExpr
		default:
			expr = parenthesize(knownExpr, parenExpr)
		}

	}

	unaryExpr := &ast.UnaryExpr{
		OpPos: -1,
		Op:    token.NOT,
		X:     expr,
	}
	if notTokenId != nil {
		t.setTokenReplaced(transformutil.TokenId{unaryExpr, 0}, *notTokenId)
	}
	return unaryExpr

}

func parenthesize(expr ast.Expr, parenExpr *ast.ParenExpr) *ast.ParenExpr {

	if parenExpr, ok := expr.(*ast.ParenExpr); ok {
		return parenExpr
	}
	if parenExpr != nil {
		parenExpr.X = expr
		return parenExpr
	}
	return &ast.ParenExpr{
		Lparen: -1,
		X:      expr,
		Rparen: -1,
	}

}

func stripParentheses(expr ast.Expr) (ast.Expr, *ast.ParenExpr) {

	if parenExpr, ok := expr.(*ast.ParenExpr); ok {
		return parenExpr.X, parenExpr
	}
	return expr, nil

}

func getCommutativeBinaryOperands(expr ast.Expr, op token.Token) ([]ast.Expr, []transformutil.TokenId) {

	if binaryExpr, ok := expr.(*ast.BinaryExpr); ok && binaryExpr.Op == op {
		operands, opTokenIds := getCommutativeBinaryOperands(binaryExpr.X, op)
		operands2, opTokenIds2 := getCommutativeBinaryOperands(binaryExpr.Y, op)
		operands = append(operands, operands2...)
		opTokenIds = append(opTokenIds, transformutil.TokenId{binaryExpr, 1})
		opTokenIds = append(opTokenIds, opTokenIds2...)
		return operands, opTokenIds
	}

	return []ast.Expr{expr}, nil

}

func (t *Transformer) reduceInts(operands []ast.Expr, reduce func (a, b int) int, identity int) (newOperands []ast.Expr) {

	res := identity
	var firstBasicLit *ast.BasicLit = nil

	for _, operand := range operands {
		exprValue, basicLit := exprToExprValue(operand)
		if intExprValue, ok := exprValue.(*intExprValue); ok && exprValue != nil {
			if firstBasicLit == nil {
				firstBasicLit = basicLit.(*ast.BasicLit)
			}
			res = reduce(res, intExprValue.value)
		} else {
			newOperands = append(newOperands, operand)
		}
	}

	if res != identity || len(operands) == 1 {
		newOperand, basicLit := exprValueToExpr(&intExprValue{value: res})
		t.setTokenReplaced(transformutil.TokenId{basicLit, 0}, transformutil.TokenId{firstBasicLit, 0})
		newOperands = append(newOperands, newOperand)
	}

	return

}

func (t *Transformer) combineToBinaryExpr(operands []ast.Expr, opTokenIds []transformutil.TokenId, op token.Token) ast.Expr {

	x := operands[0]
	nOperands := len(operands)

	if nOperands == 1 {
		return x
	}
	if nOperands > 2 {
		x = t.combineToBinaryExpr(operands[:nOperands - 1], opTokenIds, op)
	}

	binaryExpr := &ast.BinaryExpr{
		X:     x,
		OpPos: -1,
		Op:    op,
		Y:     operands[nOperands - 1],
	}
	t.setTokenReplaced(transformutil.TokenId{binaryExpr, 1}, opTokenIds[nOperands - 2])
	return binaryExpr

}

const defaultOperationPrecedence = -2
const unaryOperationPrecedence = -1
var binaryOperationToPrecedence = map[token.Token]int{
	token.MUL:     0,
	token.QUO:     0,
	token.REM:     0,
	token.SHL:     0,
	token.SHR:     0,
	token.AND:     0,
	token.AND_NOT: 0,
	token.ADD:     1,
	token.SUB:     1,
	token.OR:      1,
	token.XOR:     1,
	token.EQL:     2,
	token.NEQ:     2,
	token.LSS:     2,
	token.LEQ:     2,
	token.GTR:     2,
	token.GEQ:     2,
	token.LAND:    3,
	token.LOR:     4,
}

func getOperationPrecedence(expr ast.Expr) int {

	switch expr := expr.(type) {
	case *ast.StarExpr:
		return unaryOperationPrecedence
	case *ast.UnaryExpr:
		return unaryOperationPrecedence
	case *ast.BinaryExpr:
		return binaryOperationToPrecedence[expr.Op]
	default:
		return defaultOperationPrecedence
	}

}

type binaryOperationAssociativity uint
const (
	leftAssoc binaryOperationAssociativity = 1 << iota
	rightAssoc
)

var binaryOperationToAssociativity = map[token.Token]binaryOperationAssociativity{
	token.MUL:     leftAssoc | rightAssoc,
	token.QUO:     leftAssoc,
	token.REM:     leftAssoc,
	token.SHL:     leftAssoc,
	token.SHR:     leftAssoc,
	token.AND:     leftAssoc | rightAssoc,
	token.AND_NOT: leftAssoc,
	token.ADD:     leftAssoc | rightAssoc,
	token.SUB:     leftAssoc,
	token.OR:      leftAssoc | rightAssoc,
	token.XOR:     leftAssoc | rightAssoc,
	token.EQL:     0,
	token.NEQ:     0,
	token.LSS:     0,
	token.LEQ:     0,
	token.GTR:     0,
	token.GEQ:     0,
	token.LAND:    leftAssoc | rightAssoc,
	token.LOR:     leftAssoc | rightAssoc,
}

func isBinaryOperationLeftAssoc(op token.Token) bool {
	return binaryOperationToAssociativity[op] & leftAssoc != 0
}

func isBinaryOperationRightAssoc(op token.Token) bool {
	return binaryOperationToAssociativity[op] & rightAssoc != 0
}

var binaryOperationToCommutativity = map[token.Token]bool{
	token.MUL:     true,
	token.QUO:     false,
	token.REM:     false,
	token.SHL:     false,
	token.SHR:     false,
	token.AND:     true,
	token.AND_NOT: false,
	token.ADD:     true,
	token.SUB:     false,
	token.OR:      true,
	token.XOR:     true,
	token.EQL:     true,
	token.NEQ:     true,
	token.LSS:     false,
	token.LEQ:     false,
	token.GTR:     false,
	token.GEQ:     false,
	token.LAND:    true,
	token.LOR:     true,
}

func isBinaryOperationCommutative(op token.Token) bool {
	return binaryOperationToCommutativity[op]
}

type binaryOperationOperandTypes int
const (
	ints binaryOperationOperandTypes = iota
	bools
	intsToBool
	intsOrBoolsToBool
)

var binaryOperationToOperandType = map[token.Token]binaryOperationOperandTypes{
	token.MUL:     ints,
	token.QUO:     ints,
	token.REM:     ints,
	token.SHL:     ints,
	token.SHR:     ints,
	token.AND:     ints,
	token.AND_NOT: ints,
	token.ADD:     ints,
	token.SUB:     ints,
	token.OR:      ints,
	token.XOR:     ints,
	token.EQL:     intsOrBoolsToBool,
	token.NEQ:     intsOrBoolsToBool,
	token.LSS:     intsToBool,
	token.LEQ:     intsToBool,
	token.GTR:     intsToBool,
	token.GEQ:     intsToBool,
	token.LAND:    bools,
	token.LOR:     bools,
}

var intsBinaryOperationReduce = map[token.Token]func (a, b int) int{
	token.ADD:     func(a, b int) int { return a + b },
	token.SUB:     func(a, b int) int { return a - b },
	token.MUL:     func(a, b int) int { return a * b },
	token.QUO:     func(a, b int) int { return a / b },
	token.REM:     func(a, b int) int { return a % b },
	token.AND:     func(a, b int) int { return a & b },
	token.OR:      func(a, b int) int { return a | b },
	token.XOR:     func(a, b int) int { return a ^ b },
	token.SHL:     func(a, b int) int { return a << uint(b) },
	token.SHR:     func(a, b int) int { return a >> uint(b) },
	token.AND_NOT: func(a, b int) int { return a &^ b },
}

var boolsBinaryOperationReduce = map[token.Token]func (a, b bool) bool{
	token.LAND: func(a, b bool) bool { return a && b },
	token.LOR:  func(a, b bool) bool { return a || b },
	token.EQL:  func(a, b bool) bool { return a == b },
	token.NEQ:  func(a, b bool) bool { return a != b },
}

var intsToBoolBinaryOperationReduce = map[token.Token]func (a, b int) bool{
	token.LSS: func(a, b int) bool { return a < b },
	token.LEQ: func(a, b int) bool { return a <= b },
	token.GTR: func(a, b int) bool { return a > b },
	token.GEQ: func(a, b int) bool { return a >= b },
	token.EQL: func(a, b int) bool { return a == b },
	token.NEQ: func(a, b int) bool { return a != b },
}

type exprValue interface {
	exprValue()
}

type intExprValue struct {
	value int
}
func (*intExprValue) exprValue() {}

type boolExprValue struct {
	value bool
}
func (*boolExprValue) exprValue() {}

func exprToExprValue(expr ast.Expr) (exprValue, ast.Node) {

	basicLitExpr := expr
	intSign := 1
	if unaryExpr, ok := expr.(*ast.UnaryExpr); ok && (unaryExpr.Op == token.ADD || unaryExpr.Op == token.SUB) {
		basicLitExpr = unaryExpr.X
		if unaryExpr.Op == token.SUB {
			intSign = -1
		}
	}
	if basicLit, ok := basicLitExpr.(*ast.BasicLit); ok && basicLit.Kind == token.INT {
		if intValue, err := strconv.ParseInt(basicLit.Value, 0, 64); err == nil {
			return &intExprValue{value: intSign * int(intValue)}, basicLit
		}
	}

	if ident, ok := expr.(*ast.Ident); ok && (ident.Name == "true" || ident.Name == "false") {
		exprValue := &boolExprValue{value: true}
		if ident.Name == "false" {
			exprValue.value = false
		}
		return exprValue, ident
	}

	return nil, nil

}

func exprValueToExpr(exprValue exprValue) (ast.Expr, ast.Node) {

	switch exprValue := exprValue.(type) {
	case *intExprValue:

		absValue := exprValue.value
		if absValue < 0 {
			absValue = -absValue
		}
		basicLit := &ast.BasicLit{
			ValuePos: -1,
			Kind:     token.INT,
			Value:    strconv.FormatInt(int64(absValue), 10),
		}
		expr := ast.Expr(basicLit)
		if exprValue.value < 0 {
			expr = &ast.UnaryExpr{
				OpPos: -1,
				Op:    token.SUB,
				X:     basicLit,
			}
		}
		return expr, basicLit

	case *boolExprValue:

		name := "true"
		if !exprValue.value {
			name = "false"
		}
		ident := &ast.Ident{
			NamePos: -1,
			Name: name,
			Obj: nil,
		}
		return ident, ident

	default:
		panic("Unreachable code")

	}

}

var intsBinaryOperationToIdentity = map[token.Token]int{
	token.ADD:     0,
	token.SUB:     0,
	token.MUL:     1,
	token.QUO:     1,
	token.REM:     1,
	token.AND:     -1,
	token.OR:      0,
	token.SHL:     0,
	token.SHR:     0,
	token.AND_NOT: 0,
}

var intsBinaryOperationToZero = map[token.Token]int{
	token.MUL:     0,
	token.QUO:     0,
	token.REM:     0,
	token.AND:     0,
	token.OR:      -1,
	token.SHL:     0,
	token.SHR:     0,
	token.AND_NOT: 0,
}

var boolsBinaryOperationToIdentity = map[token.Token]bool{
	token.LAND: true,
	token.LOR:  false,
}

var boolsBinaryOperationToZero = map[token.Token]bool{
	token.LAND: false,
	token.LOR:  true,
}
package transform

import (
	"bytes"
	"go/ast"
	"go/printer"
	"go/scanner"
	"go/token"
)

type Token struct {
	Pos token.Pos
	Tok token.Token
	Lit string
	Line int
	Column int
}

func TokenizeSource(file *token.File, src []byte) []Token {

	var tokens []Token

	var s scanner.Scanner
	s.Init(file, src, nil, 0)

	for {
		pos, tok, lit := s.Scan()
		if tok == token.EOF {
			break
		}
		if tok == token.SEMICOLON {
			continue
		}
		position := file.Position(pos)
		tokens = append(tokens, Token{
			Pos:    pos,
			Tok:    tok,
			Lit:    lit,
			Line:   position.Line,
			Column: position.Column,
		})
	}

	return tokens

}

func TokenizeTree(fileSet *token.FileSet, tree *ast.File) []Token {

	buffer := &bytes.Buffer{}
	_ = printer.Fprint(buffer, fileSet, tree)
	src := buffer.Bytes()

	fileSet = token.NewFileSet()
	file := fileSet.AddFile("dummy.go", fileSet.Base(), len(src))
	return TokenizeSource(file, src)

}
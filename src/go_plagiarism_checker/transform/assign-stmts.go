package transform

import (
	"go/ast"
	"go/token"
	"go_plagiarism_checker/transformutil"
)

func (t *Transformer) transformAssignStmts(stmtListPtr *[]ast.Stmt) {

	for i := len(*stmtListPtr) - 1; i >= 0; i-- {
		switch stmt := (*stmtListPtr)[i].(type) {

		case *ast.AssignStmt:

			if len(stmt.Lhs) > 1 && len(stmt.Lhs) == len(stmt.Rhs) {

				// Splitting multi-variable (parallel) assignment
				for j := len(stmt.Lhs) - 1; j >= 1; j-- {
					// Creating new AssignStmt
					newAssignStmt := &ast.AssignStmt{
						Lhs:    []ast.Expr{stmt.Lhs[j]},
						TokPos: -1,
						Tok:    stmt.Tok,
						Rhs:    []ast.Expr{stmt.Rhs[j]},
					}
					*stmtListPtr = append((*stmtListPtr)[:i+1],
						append([]ast.Stmt{newAssignStmt}, (*stmtListPtr)[i+1:]...)...)
				}

				stmt.Lhs = stmt.Lhs[:1]
				stmt.Rhs = stmt.Rhs[:1]

			} else if stmt.Tok != token.DEFINE && stmt.Tok != token.ASSIGN {

				// `x += y` -> `x = x + y`
				var op token.Token
				switch stmt.Tok {
				case token.ADD_ASSIGN:
					op = token.ADD
				case token.SUB_ASSIGN:
					op = token.SUB
				case token.MUL_ASSIGN:
					op = token.MUL
				case token.QUO_ASSIGN:
					op = token.QUO
				case token.REM_ASSIGN:
					op = token.REM
				case token.AND_ASSIGN:
					op = token.AND
				case token.OR_ASSIGN:
					op = token.OR
				case token.XOR_ASSIGN:
					op = token.XOR
				case token.SHL_ASSIGN:
					op = token.SHL
				case token.SHR_ASSIGN:
					op = token.SHR
				case token.AND_NOT_ASSIGN:
					op = token.AND_NOT
				default:
					panic("Unknown token in AssignStmt")
				}

				stmt.Tok = token.ASSIGN
				stmt.Rhs = []ast.Expr{&ast.BinaryExpr{
					X:     stmt.Lhs[0],
					OpPos: -1,
					Op:    op,
					Y:     transformutil.ParenthesizeIfBinary(stmt.Rhs[0]),
				}}

			}

		case *ast.IncDecStmt:

			// `x++` -> `x = x + 1`
			op := token.ADD
			if stmt.Tok == token.DEC {
				op = token.SUB
			}

			assignStmt := &ast.AssignStmt{
				Lhs:    []ast.Expr{stmt.X},
				TokPos: -1,
				Tok:    token.ASSIGN,
				Rhs: []ast.Expr{&ast.BinaryExpr{
					X:     stmt.X,
					OpPos: -1,
					Op:    op,
					Y: &ast.BasicLit{
						ValuePos: -1,
						Kind:     token.INT,
						Value:    "1",
					},
				}},
			}
			t.setTokenReplaced(transformutil.TokenId{assignStmt, 1}, transformutil.TokenId{stmt, 1})

			(*stmtListPtr)[i] = assignStmt

		}
	}

}
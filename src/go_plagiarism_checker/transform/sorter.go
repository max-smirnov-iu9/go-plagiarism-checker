package transform

import (
	"go/ast"
	"go_plagiarism_checker/transformutil"
	"sort"
)

type sortableStmtList []ast.Stmt

func (s sortableStmtList) Len() int {
	return len(s)
}

func (s sortableStmtList) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s sortableStmtList) Less(i, j int) bool {
	return transformutil.CompareNodes(s[i], s[j]) < 0
}

func sortStmtList(stmtList []ast.Stmt) {
	sort.Sort(sortableStmtList(stmtList))
}

type sortableDeclList []ast.Decl

func (s sortableDeclList) Len() int {
	return len(s)
}

func (s sortableDeclList) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s sortableDeclList) Less(i, j int) bool {
	return transformutil.CompareNodes(s[i], s[j]) < 0
}

func sortDeclList(declList []ast.Decl) {
	sort.Sort(sortableDeclList(declList))
}

type sortableSpecList []ast.Spec

func (s sortableSpecList) Len() int {
	return len(s)
}

func (s sortableSpecList) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s sortableSpecList) Less(i, j int) bool {
	return transformutil.CompareNodes(s[i], s[j]) < 0
}

func sortSpecList(specList []ast.Spec) {
	sort.Sort(sortableSpecList(specList))
}

type sortableExprList []ast.Expr

func (s sortableExprList) Len() int {
	return len(s)
}

func (s sortableExprList) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s sortableExprList) Less(i, j int) bool {
	return transformutil.CompareNodes(s[i], s[j]) < 0
}

func sortExprList(exprList []ast.Expr) {
	sort.Sort(sortableExprList(exprList))
}
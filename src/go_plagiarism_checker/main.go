package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/umpc/go-sortedmap"
	"github.com/umpc/go-sortedmap/asc"
	"github.com/umpc/go-sortedmap/desc"
	"go_plagiarism_checker/comparator"
	"go_plagiarism_checker/transform"
	"go_plagiarism_checker/visualizer"
	"io/ioutil"
	"os"
	"path"
	"strings"
	"text/tabwriter"
	"time"
)

const similaritiesDir = "result"
const transformationsDir = "transform"
const csvFilename = "result.csv"
const csvAlphabeticalFilename = "result-alphabetical.csv"

func writeCsv(path string, sortedMap *sortedmap.SortedMap) {

	csvFile, err := os.Create(path)
	if err != nil {
		fmt.Printf("os.Create error: %v", err)
		return
	}
	defer csvFile.Close()

	// Writing out UTF-8 BOM
	_, err = csvFile.Write([]byte{0xEF, 0xBB, 0xBF})
	if err != nil {
		fmt.Printf("csvWriter.Write error: %v", err)
		return
	}

	csvWriter := csv.NewWriter(csvFile)
	sortedMap.IterFunc(false, func(rec sortedmap.Record) bool {
		row := rec.Key.([3]string)
		err = csvWriter.Write(row[:])
		if err != nil {
			fmt.Printf("csvWriter.Write error: %v", err)
			return false
		}
		return true
	})
	csvWriter.Flush()

}

func main() {

	spew.Config.Indent = "\t"

	/* Reading CLI arguments */

	var (
		outputDir, compareConsiderEqualStr, enabledTransformsStr                       string
		compareBlockSize                                                               int
		percentageLimit                                                                float64
		csvResult, debugCsvResults, visualizeSimilarities, visualizeTransform, verbose bool
	)

	flag.StringVar(&outputDir, "out-dir", ".", "output directory")
	flag.IntVar(&compareBlockSize, "k", 30, "block size: minimum number of consecutive matching tokens to report")
	flag.Float64Var(&percentageLimit, "limit", 50, "minimum percentage of plagiarism between reported files")
	flag.BoolVar(&csvResult, "csv-result", true, "output result summary as CSV")
	flag.BoolVar(&debugCsvResults, "csv-result-debug", false, "")
	flag.BoolVar(&visualizeSimilarities, "vis-result", false, "visualize comparison results as HTML for each reported file pair")
	flag.BoolVar(&visualizeTransform, "vis-transform", false, "visualize transformation results as HTML for each file")
	flag.BoolVar(&verbose, "verbose", false, "verbose output")
	flag.StringVar(&compareConsiderEqualStr, "consider-equal", "all", "which literals to consider equal, comma separated; possible values: idents, numbers, chars, strings, none, all")
	flag.StringVar(&enabledTransformsStr, "enabled-transforms", "all", "which transformations to apply; possible values: var-blocks, assign-stmts, sort, exprs, control-stmts, none, all")
	flag.Parse()

	compareConsiderEqual := comparator.ConsiderEqual(0)
	for _, part := range strings.Split(compareConsiderEqualStr, ",") {
		switch part {
		case "all":
			compareConsiderEqual = comparator.Idents | comparator.Numbers | comparator.Chars | comparator.Strings
		case "idents":
			compareConsiderEqual |= comparator.Idents
		case "numbers":
			compareConsiderEqual |= comparator.Numbers
		case "chars":
			compareConsiderEqual |= comparator.Chars
		case "strings":
			compareConsiderEqual |= comparator.Strings
		}
	}

	enabledTransforms := transform.EnabledTransforms(0)
	for _, part := range strings.Split(enabledTransformsStr, ",") {
		switch part {
		case "all":
			enabledTransforms = transform.VarBlocks | transform.AssignStmts | transform.Sort | transform.Exprs | transform.ControlStmts
		case "var-blocks":
			enabledTransforms |= transform.VarBlocks
		case "assign-stmts":
			enabledTransforms |= transform.AssignStmts
		case "sort":
			enabledTransforms |= transform.Sort
		case "exprs":
			enabledTransforms |= transform.Exprs
		case "control-stmts":
			enabledTransforms |= transform.ControlStmts
		}
	}

	/* Collecting files */

	paths := flag.Args()
	pathsErrorText := "Please specify either a directory, two or more files or a single file with -vis-transform flag"
	if len(paths) == 0 {
		fmt.Println(pathsErrorText)
		return
	}

	var fileNames []string
	var filePaths []string
	stat, err := os.Stat(paths[0])
	if err != nil {
		fmt.Printf("os.Stat error: %v\n", err)
		return
	}
	if !stat.Mode().IsDir() {
		if len(paths) == 1 && !visualizeTransform {
			fmt.Println(pathsErrorText)
			return
		}
		filePaths = paths
		fileNames = make([]string, len(filePaths))
		for i, filePath := range filePaths {
			fileNames[i] = path.Base(filePath)
		}
	} else {
		filesList, err := ioutil.ReadDir(paths[0])
		if err != nil {
			fmt.Printf("ioutil.ReadDir error: %v\n", err)
			return
		}
		fileNames = make([]string, 0, len(filesList))
		filePaths = make([]string, 0, len(filesList))
		for _, f := range filesList {
			if !f.IsDir() {
				fileNames = append(fileNames, f.Name())
				filePaths = append(filePaths, path.Join(paths[0], f.Name()))
			}
		}
	}

	_ = os.Mkdir(outputDir, os.ModeDir)
	if visualizeTransform {
		_ = os.Mkdir(path.Join(outputDir, transformationsDir), os.ModeDir)
	}
	if visualizeSimilarities {
		_ = os.Mkdir(path.Join(outputDir, similaritiesDir), os.ModeDir)
	}

	/* Transforming */

	nSources := len(fileNames)
	originalTokens := make([][]transform.Token, nSources)
	originalLines := make([][]string, nSources)
	tokens := make([][]transform.Token, nSources)
	indexMaps := make([]map[int]int, nSources)

	execStart := time.Now()
	transformStart := time.Now()

	for i, file := range fileNames {

		if verbose {
			fmt.Printf("Transforming: %s...\n", file)
		}

		transformer := &transform.Transformer{EnabledTransforms: enabledTransforms}
		transformer.Init(filePaths[i])
		originalTokens[i] = transformer.Tokens
		originalLines[i] = transformer.Lines

		transformer.Transform()
		tokens[i] = transformer.Tokens
		lines := transformer.Lines
		indexMaps[i] = transformer.TokenMap

		if visualizeTransform {
			f, err := os.Create(path.Join(outputDir, transformationsDir, fmt.Sprintf("%s.html", file)))
			if err != nil {
				fmt.Printf("os.Create error: %v\n", err)
			}
			visualizer.VisualizeTransformation(originalLines[i], lines, originalTokens[i], tokens[i], transformer.TokenMap, f)
			f.Close()
		}

	}

	transformDuration := time.Since(transformStart)

	/* Comparing */

	writeDuration := time.Duration(0)
	compareDuration := time.Duration(0)

	if nSources > 1 {

		if verbose {
			fmt.Println("Comparing...")
		}
		compareStart := time.Now()

		comparator := &comparator.Comparator{
			BlockSize:     compareBlockSize,
			ConsiderEqual: compareConsiderEqual,
		}
		comparisonResults := comparator.CompareMultiple(tokens, indexMaps)

		compareDuration = time.Since(compareStart)

		/* Writing out comparison results */

		writeStart := time.Now()

		totalResults := nSources * (nSources - 1) / 2
		results := sortedmap.New(totalResults, desc.Float64)
		resultsAlphabetical := sortedmap.New(totalResults, asc.Int)
		for k1, comparisonResults1 := range comparisonResults {
			for k2, comparisonResult := range comparisonResults1 {
				if comparisonResult.Percentage >= percentageLimit {

					if visualizeSimilarities {
						f, err := os.Create(path.Join(outputDir, similaritiesDir,
							fmt.Sprintf("%s.%s.html", fileNames[k1], fileNames[k2])))
						if err != nil {
							fmt.Printf("os.Create error: %v\n", err)
							continue
						}
						visualizer.VisualizeSimilarities(originalLines[k1], originalLines[k2], originalTokens[k1], originalTokens[k2],
							comparisonResult.SimilarBlocks, f)
						f.Close()
					}

					if csvResult {
						row := [3]string{fmt.Sprintf("%.2f", comparisonResult.Percentage), fileNames[k1], fileNames[k2]}
						results.Insert(row, comparisonResult.Percentage)
						if debugCsvResults {
							resultsAlphabetical.Insert(row, k1 * nSources + k2)
						}
					}

				}
			}
		}

		if csvResult {
			writeCsv(path.Join(outputDir, csvFilename), results)
			if debugCsvResults {
				writeCsv(path.Join(outputDir, csvAlphabeticalFilename), resultsAlphabetical)
			}
		}

		writeDuration = time.Since(writeStart)

	}

	totalExecDuration := time.Since(execStart)
	if verbose {
		w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)
		_, _ = fmt.Fprintf(w, "Transformations took:\t%s\n", transformDuration)
		_, _ = fmt.Fprintf(w, "Comparison took:\t%s\n", compareDuration)
		_, _ = fmt.Fprintf(w, "Writing out comparison results took:\t%s\n", writeDuration)
		_, _ = fmt.Fprintf(w, "Total execution time:\t%s\n", totalExecDuration)
		_ = w.Flush()
	}

}

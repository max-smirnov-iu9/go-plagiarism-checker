package comparator

import (
	"crypto/md5"
	"go/token"
	"go_plagiarism_checker/transform"
	"math"
)

type ConsiderEqual uint
const (
	Idents ConsiderEqual = 1 << iota
	Numbers
	Chars
	Strings
)

type Comparator struct {
	BlockSize     int
	ConsiderEqual ConsiderEqual
}

type hash [md5.Size]byte

func (c *Comparator) blockToBinary(tokens []transform.Token) []byte {

	data := make([]byte, 0, c.BlockSize)

	for j := 0; j < c.BlockSize; j++ {

		curToken := tokens[j]

		writeLit := false
		switch curToken.Tok {
		case token.IDENT:
			writeLit = c.ConsiderEqual &Idents == 0
		case token.INT, token.FLOAT, token.IMAG:
			writeLit = c.ConsiderEqual &Numbers == 0
		case token.CHAR:
			writeLit = c.ConsiderEqual &Chars == 0
		case token.STRING:
			writeLit = c.ConsiderEqual &Strings == 0
		}

		data = append(data, byte(curToken.Tok))
		if writeLit {
			l := uint(len(curToken.Lit))
			data = append(data, byte(l), byte(l >> 8), byte(l >> 16), byte(l >> 24))
			data = append(data, []byte(curToken.Lit)...)
		}

	}

	return data

}

func translateIndicesBack(indices map[int]bool, indexMap map[int]int) map[int]bool {

	translatedIndices := make(map[int]bool)
	for to := range indices {
		if from, ok := indexMap[to]; ok {
			translatedIndices[from] = true
		}
	}
	return translatedIndices

}

type SimilarBlocks struct {
	transformedStart1 int
	transformedEnd1   int
	transformedStart2 int
	transformedEnd2   int
	Indices1          map[int]bool
	Indices2          map[int]bool
}

type ComparisonResult struct {
	SimilarBlocks       []SimilarBlocks
	highlightedIndices1 map[int]bool
	highlightedIndices2 map[int]bool
	Percentage          float64
}

func (c *Comparator) writeBlockToIndexSet(indexSet map[int]bool, newIndex int) {

	for i := 0; i < c.BlockSize; i++ {
		indexSet[newIndex + i] = true
	}

}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func (c *Comparator) updateSimilarBlocks(result *ComparisonResult, hashIndex1, hashIndex2 int) {

	c.writeBlockToIndexSet(result.highlightedIndices1, hashIndex1)
	c.writeBlockToIndexSet(result.highlightedIndices2, hashIndex2)

	start1 := hashIndex1
	end1 := start1 + c.BlockSize
	start2 := hashIndex2
	end2 := start2 + c.BlockSize

	for i, similarBlocks := range result.SimilarBlocks {
		if end1 >= similarBlocks.transformedStart1 && end1 <= similarBlocks.transformedEnd1 &&
				end2 >= similarBlocks.transformedStart2 && end2 <= similarBlocks.transformedEnd2 {
			result.SimilarBlocks[i].transformedStart1 = min(result.SimilarBlocks[i].transformedStart1, start1)
			result.SimilarBlocks[i].transformedStart2 = min(result.SimilarBlocks[i].transformedStart2, start2)
			return
		} else if start1 >= similarBlocks.transformedStart1 && start1 <= similarBlocks.transformedEnd1 &&
				start2 >= similarBlocks.transformedStart2 && start2 <= similarBlocks.transformedEnd2 {
			result.SimilarBlocks[i].transformedEnd1 = max(result.SimilarBlocks[i].transformedEnd1, end1)
			result.SimilarBlocks[i].transformedEnd2 = max(result.SimilarBlocks[i].transformedEnd2, end2)
			return
		}
	}

	result.SimilarBlocks = append(result.SimilarBlocks, SimilarBlocks{
		transformedStart1: start1,
		transformedEnd1:   end1,
		transformedStart2: start2,
		transformedEnd2:   end2,
	})

}

func (c *Comparator) CompareMultiple(tokens [][]transform.Token, indexMaps []map[int]int) [][]ComparisonResult {

	nSources := len(tokens)

	// Each hash corresponds to a slice of indices.
	// That's because there could be multiple occurrences of identical blocks in a file.
	hashToHashIndexPerFile := make([]map[hash][]int, nSources)

	// Hash -> set of file indices
	allHashesToFileIndices := make(map[hash]map[int]bool)

	for k := 0; k < nSources; k++ {
		hashToHashIndexPerFile[k] = make(map[hash][]int)
		for i := 0; i <= len(tokens[k]) - c.BlockSize; i++ {
			data := c.blockToBinary(tokens[k][i:])
			hashSum := md5.Sum(data)
			hashToHashIndexPerFile[k][hashSum] = append(hashToHashIndexPerFile[k][hashSum], i)
			if allHashesToFileIndices[hashSum] == nil {
				allHashesToFileIndices[hashSum] = make(map[int]bool)
			}
			allHashesToFileIndices[hashSum][k] = true
		}
	}

	comparisonResults := make([][]ComparisonResult, nSources)
	for k1 := 0; k1 < nSources; k1++ {
		comparisonResults[k1] = make([]ComparisonResult, k1)
		for k2 := 0; k2 < k1; k2++ {
			comparisonResults[k1][k2] = ComparisonResult{
				highlightedIndices1: make(map[int]bool),
				highlightedIndices2: make(map[int]bool),
			}
		}
	}

	for hashSum, fileIndexSet := range allHashesToFileIndices {
		for fileIndex1 := range fileIndexSet {
			hashIndices1 := hashToHashIndexPerFile[fileIndex1][hashSum]
			for fileIndex2 := range fileIndexSet {
				if fileIndex2 < fileIndex1 {
					hashIndices2 := hashToHashIndexPerFile[fileIndex2][hashSum]
					len1, len2 := len(hashIndices1), len(hashIndices2)
					for i := 0; i < min(len1, len2); i++ {
						c.updateSimilarBlocks(&comparisonResults[fileIndex1][fileIndex2], hashIndices1[i], hashIndices2[i])
					}
					if len1 < len2 {
						for i := len1; i < len2; i++ {
							c.updateSimilarBlocks(&comparisonResults[fileIndex1][fileIndex2], hashIndices1[len1 - 1], hashIndices2[i])
						}
					} else if len2 < len1 {
						for i := len2; i < len1; i++ {
							c.updateSimilarBlocks(&comparisonResults[fileIndex1][fileIndex2], hashIndices1[i], hashIndices2[len2 - 1])
						}
					}
				}
			}
		}
	}

	for k1 := 0; k1 < nSources; k1++ {
		for k2 := 0; k2 < k1; k2++ {
			comparisonResults[k1][k2].Percentage = math.Max(
				float64(len(comparisonResults[k1][k2].highlightedIndices1)) / float64(len(tokens[k1])),
				float64(len(comparisonResults[k1][k2].highlightedIndices2)) / float64(len(tokens[k2]))) * 100
			for i, similarBlocks := range comparisonResults[k1][k2].SimilarBlocks {

				indices1 := make(map[int]bool)
				for j := similarBlocks.transformedStart1; j < similarBlocks.transformedEnd1; j++ {
					indices1[j] = true
				}
				comparisonResults[k1][k2].SimilarBlocks[i].Indices1 = translateIndicesBack(indices1, indexMaps[k1])

				indices2 := make(map[int]bool)
				for j := similarBlocks.transformedStart2; j < similarBlocks.transformedEnd2; j++ {
					indices2[j] = true
				}
				comparisonResults[k1][k2].SimilarBlocks[i].Indices2 = translateIndicesBack(indices2, indexMaps[k2])

			}
		}
	}

	return comparisonResults

}
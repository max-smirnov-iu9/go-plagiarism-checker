package visualizer

import (
	"fmt"
	"go_plagiarism_checker/comparator"
	"go_plagiarism_checker/transform"
	"html/template"
	"io"
	"math/rand"
	"os"
	"path"
	"path/filepath"
	"sort"
)

type selectionRange struct {
	Text string
	Color template.CSS
}

type line struct {
	Number int
	SelectionRanges []selectionRange
}

type source struct {
	Lines []*line
}

type diff struct {
	Source1 *source
	Source2 *source
}

func substringToken(lineStr string, tokens []transform.Token, index int) string {
	var start int
	if index == 0 || tokens[index - 1].Line != tokens[index].Line {
		start = 0
	} else {
		start = tokens[index].Column - 1
	}
	var end int
	if index == len(tokens) - 1 || tokens[index + 1].Line != tokens[index].Line {
		end = len(lineStr)
	} else {
		end = tokens[index + 1].Column - 1
	}
	return lineStr[start:end]
}

const defaultColor = template.CSS("inherit")

func generateRandomColor() template.CSS {
	return template.CSS(fmt.Sprintf("hsl(%d, 100%%, 89%%)", rand.Intn(360)))
}

func VisualizeTransformation(linesFrom, linesTo []string, tokensFrom, tokensTo []transform.Token, indexMap map[int]int, writer io.Writer) {

	sourceFrom := &source{}
	sourceToTokenIndexToColor := make(map[int]template.CSS)
	// sourceToLineIndexToSelectionRanges := make(map[int][]selectionRange)

	lastTokenIndex := 0
	color := generateRandomColor()
	lastCorrespondingToTokenIndex := -1
	for i, lineStr := range linesFrom {

		nextLine := &line{Number: i + 1}
		sourceFrom.Lines = append(sourceFrom.Lines, nextLine)

		text := ""
		for lastTokenIndex < len(tokensFrom) && tokensFrom[lastTokenIndex].Line == i + 1 {

			var correspondingToTokenIndices []int
			for to, from := range indexMap {
				if from == lastTokenIndex {
					correspondingToTokenIndices = append(correspondingToTokenIndices, to)
				}
			}
			sort.Ints(correspondingToTokenIndices)

			if len(correspondingToTokenIndices) == 0 || lastCorrespondingToTokenIndex + 1 != correspondingToTokenIndices[0] {
				nextLine.SelectionRanges = append(nextLine.SelectionRanges, selectionRange{
					Text:  text,
					Color: color,
				})
				text = ""
				color = defaultColor
				if len(correspondingToTokenIndices) != 0 {
					color = generateRandomColor()
				} else {
					lastCorrespondingToTokenIndex = -1
				}
			}
			text += substringToken(lineStr, tokensFrom, lastTokenIndex)
			for _, correspondingToTokenIndex := range correspondingToTokenIndices {
				sourceToTokenIndexToColor[correspondingToTokenIndex] = color
			}
			if len(correspondingToTokenIndices) != 0 {
				lastCorrespondingToTokenIndex = correspondingToTokenIndices[len(correspondingToTokenIndices) - 1]
			}

			lastTokenIndex++

		}
		nextLine.SelectionRanges = append(nextLine.SelectionRanges, selectionRange{
			Text:  text,
			Color: color,
		})

	}

	sourceTo := &source{}

	lastTokenIndex = 0
	for i, lineStr := range linesTo {

		nextLine := &line{Number: i + 1}
		sourceTo.Lines = append(sourceTo.Lines, nextLine)

		for lastTokenIndex < len(tokensTo) && tokensTo[lastTokenIndex].Line == i + 1 {

			text := substringToken(lineStr, tokensTo, lastTokenIndex)
			color := defaultColor
			if storedColor, ok := sourceToTokenIndexToColor[lastTokenIndex]; ok {
				color = storedColor
			}
			nextLine.SelectionRanges = append(nextLine.SelectionRanges, selectionRange{
				Text:  text,
				Color: color,
			})

			lastTokenIndex++

		}

	}

	templates, err := template.ParseGlob("templates/*")
	if err != nil {
		fmt.Printf("template.ParseFiles error: %v\n", err)
	}
	err = templates.ExecuteTemplate(writer, "two-sources.html", &diff{
		Source1: sourceFrom,
		Source2: sourceTo,
	})
	if err != nil {
		fmt.Printf("templates.Execute error: %v\n", err)
	}

}

var similarityColors = [...]template.CSS {
	"#D4E5F1",
	"#D5EEDF",
	"#F5DCD0",
	"#F9E4D5",
	"#FCF3D5",
	"#FCF3D5",
	"#FBE5E5",
	"#FBE5E5",
}

func getSourceWithHighlightedTokens(lines []string, tokens []transform.Token, similarBlocks []comparator.SimilarBlocks,
		similarBlocksToIndices func (similarBlocks *comparator.SimilarBlocks) map[int]bool) *source {

	source := &source{}

	lastTokenIndex := 0
	for i, lineStr := range lines {

		nextLine := &line{Number: i + 1}
		source.Lines = append(source.Lines, nextLine)

		// Line is not empty, but has no tokens — either has a comment or consists of whitespace
		if len(lineStr) > 0 && (lastTokenIndex >= len(tokens) || tokens[lastTokenIndex].Line > i + 1) {

			nextLine.SelectionRanges = append(nextLine.SelectionRanges, selectionRange{
				Text:  lineStr,
				Color: defaultColor,
			})

		}

		for lastTokenIndex < len(tokens) && tokens[lastTokenIndex].Line == i + 1 {

			text := substringToken(lineStr, tokens, lastTokenIndex)
			color := defaultColor
			for j, similarBlockPair := range similarBlocks {
				if _, ok := similarBlocksToIndices(&similarBlockPair)[lastTokenIndex]; ok {
					color = similarityColors[j % len(similarityColors)]
					break
				}
			}

			nextLine.SelectionRanges = append(nextLine.SelectionRanges, selectionRange{
				Text:  text,
				Color: color,
			})

			lastTokenIndex++

		}

	}

	return source

}

func VisualizeSimilarities(lines1, lines2 []string, tokens1, tokens2 []transform.Token, similarBlocks []comparator.SimilarBlocks, writer io.Writer) {

	source1 := getSourceWithHighlightedTokens(lines1, tokens1, similarBlocks, func (similarBlocks *comparator.SimilarBlocks) map[int]bool {
		return similarBlocks.Indices1
	})
	source2 := getSourceWithHighlightedTokens(lines2, tokens2, similarBlocks, func (similarBlocks *comparator.SimilarBlocks) map[int]bool {
		return similarBlocks.Indices2
	})

	dir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	templates, err := template.ParseGlob(path.Join(dir, "templates/*"))
	if err != nil {
		fmt.Printf("template.ParseFiles error: %v\n", err)
	}
	err = templates.ExecuteTemplate(writer, "two-sources.html", &diff{
		Source1: source1,
		Source2: source2,
	})
	if err != nil {
		fmt.Printf("templates.Execute error: %v\n", err)
	}

}